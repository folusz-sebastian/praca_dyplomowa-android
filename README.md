# Android app for restaurant management system mobile app project

## Opis projektu
Repozytorium jest częścią [projektu](https://bitbucket.org/folusz-sebastian/workspace/projects/RMSMA) będącego aplikacją mobilną systememu zarządzania restauracją.

Aplikacja została napisana w języku Java.

Przykładowy ekran aplikacji:  
![app_design_example_screen.jpg](readmeFiles/app_design_example_screen.jpg)
 
[podręcznik użytkownika](https://bitbucket.org/folusz-sebastian/praca_dyplomowa-android/src/master/readmeFiles/user_guide.pdf)