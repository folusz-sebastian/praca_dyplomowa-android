package com.companysf.cardview_slider.views;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.companysf.cardview_slider.R;

public class OrdersInfoBar extends RelativeLayout {
    private TextView orderPrice, orderItemsCount;
    private Context context;

    public OrdersInfoBar(Context context) {
        super(context);
        init(context, null);
    }

    public OrdersInfoBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_orders_info_bar, null);
        addView(view);

        this.context = context;
        orderPrice = view.findViewById(R.id.orderPrice);
        orderItemsCount = view.findViewById(R.id.orderItemsCount);
    }

    public void setData(int orderItemsSize, double price) {
        String totalPriceWithCurrency = this.context.getString(
                R.string.price_with_currency,
                price
        );
        String orderItemsSizeString = this.context.getString(R.string.order_items_size, orderItemsSize);

        this.orderPrice.setText(totalPriceWithCurrency);
        this.orderItemsCount.setText(orderItemsSizeString);
    }
}
