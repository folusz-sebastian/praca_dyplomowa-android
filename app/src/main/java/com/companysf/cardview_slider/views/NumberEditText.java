package com.companysf.cardview_slider.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.utils.CurrencyTextWatcher;

import java.util.Objects;

public class NumberEditText extends AppCompatEditText {
    private static final int MAX_LENGTH = 20;
    private static final int MAX_DECIMAL_DIGIT = 2;
    private String prefix;
    private CurrencyTextWatcher currencyTextWatcher;

    public NumberEditText(Context context) {
        this(context, null);
    }

    public NumberEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.editTextStyle);
    }

    public NumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        this.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH)});

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NumberEditText);
        try {
            this.prefix = typedArray.getString(R.styleable.NumberEditText_prefix);
        } finally {
            typedArray.recycle();
        }

        currencyTextWatcher = new CurrencyTextWatcher(this, prefix, MAX_LENGTH, MAX_DECIMAL_DIGIT);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            this.addTextChangedListener(currencyTextWatcher);
        } else {
            this.removeTextChangedListener(currencyTextWatcher);
        }
        handleCaseCurrencyEmpty(focused);
    }

    private void handleCaseCurrencyEmpty(boolean focused) {
        if (focused) {
            if (getText().toString().isEmpty()) {
                setText(prefix);
            }
        } else {
            if (getText().toString().equals(prefix)) {
                setText("");
            }
        }
    }

    public Double convertToDouble() {
        return Double.valueOf(
                Objects.requireNonNull(getText())
                        .toString()
                        .substring(prefix.length())
                        .replace(",", "")
        );
    }
}