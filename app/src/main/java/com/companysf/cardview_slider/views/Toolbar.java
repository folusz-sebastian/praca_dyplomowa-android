package com.companysf.cardview_slider.views;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.companysf.cardview_slider.R;

public class Toolbar extends AppBarLayout {

    public Toolbar(Context context) {
        super(context);
        init(context, null);
    }

    public Toolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_toolbar, null);
        addView(view);
    }

    public void setToolbar(AppCompatActivity activity) {
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
    }

    public void enableToolbarUpNavigation(AppCompatActivity activity) {
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
