package com.companysf.cardview_slider.app;

public class AppConfig {
    private static String websiteIp = "http://192.168.0.18:5050";

    public static String restaurantTablesUrl = websiteIp + "/api/tables";
    public static String freeRestaurantTablesUrl = websiteIp + "/api/free_tables";
    public static String reservationsUrl = websiteIp + "/api/reservations";
    public static String myReservationsUrl = websiteIp + "/api/my_reservations";
    public static String dishesUrl = websiteIp + "/api/dishes";
    public static String orderUrl = websiteIp + "/api/orders";
    public static String lastUserTransactionsUrl = websiteIp + "/api/ordered_dishes?by_user=true";
    public static String userRegistrationUrl = websiteIp + "/api/signup";
    public static String userSignInUrl = websiteIp + "/api/user";
    public static String logoutUserUrl = websiteIp + "/logout";
    public static String ordersNotPrepared = websiteIp + "/api/orders?fromToday=true";
    public static String dishCategories = websiteIp + "/api/dishCategories";
    public static String measurementUnits = websiteIp + "/api/measurementUnits";

    //NUMBER OF ELEMENTS ON LISTS
    public static final int NUMBER_OF_DISHES_ON_MAIN_SITE = 9;
    public static final int NUMBER_OF_LAST_TRANSACTIONS_ON_MAIN_SITE = 5;

    //    DICTIONARY
    //    1.dish status
    public static final String STATUS_DISH__IS_BEING_PREPARED = "przygotowywane";
    public static final String STATUS_DISH_IS_MADE = "wykonane";

    //    2.dish category
    public static final String DISH_CATEGORY_PIZZA = "pizza";
    public static final String DISH_CATEGORY_FISH = "ryba";
    public static final String DISH_CATEGORY_DINNER = "danie obiadowe";
    public static final String DISH_CATEGORY_SOUP = "zupa";
    public static final String DISH_CATEGORY_DESSERT = "deser";

    //    3.user roles
    public static final String USER_ROLE_EMPLOYEE = "PRACOWNIK";
    public static final String USER_ROLE_CUSTOMER = "KLIENT";

}
