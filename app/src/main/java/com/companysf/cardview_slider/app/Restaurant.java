package com.companysf.cardview_slider.app;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    public static final int STARTING_JOB_HOUR = 8;
    public static final int ENDING_JOB_HOUR = 20;

    public static List<String> hoursList(String suffix) {
        List<String> list = new ArrayList<>();
        for (int i = STARTING_JOB_HOUR;
             i < ENDING_JOB_HOUR ; i++) {
            list.add(i + suffix);
        }
        return list;
    }

    public static List<Integer> hoursList() {
        List<Integer> list = new ArrayList<>();
        for (int i = STARTING_JOB_HOUR;
             i < ENDING_JOB_HOUR ; i++) {
            list.add(i);
        }
        return list;
    }
}
