package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.connection.RegisterConnection;
import com.companysf.cardview_slider.entity.model.user.RegisteredUser;

public class RegisterService {
    private RegisteredUser user;
    private RegisterConnection connection;

    public RegisterService(RegisteredUser user, RegisterConnection.RegisterListener listener) {
        this.user = user;
        this.connection = new RegisterConnection(listener);
    }

    public void signUpUser() {
        this.connection.save(user);
    }
}
