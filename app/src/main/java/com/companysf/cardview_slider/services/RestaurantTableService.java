package com.companysf.cardview_slider.services;

import android.support.v7.widget.RecyclerView;

import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;

import java.util.List;

public class RestaurantTableService {
    private RestaurantTablesConnection restaurantTablesConnection;
    private List<RestaurantTable> restaurantTableList;
    private RecyclerView.Adapter adapter;

    RestaurantTableService(RestaurantTablesConnection restaurantTablesConnection,
                           List<RestaurantTable> restaurantTableList,
                           RecyclerView.Adapter adapter) {
        this.restaurantTablesConnection = restaurantTablesConnection;
        this.restaurantTableList = restaurantTableList;
        this.adapter = adapter;
    }

    public void retrieveRestaurantTables() {
        restaurantTablesConnection.findAll();
    }

    void retrieveFreeRestaurantTables(
            String year, String month, String day, int beginningHour, int stayDuration
    ) {
        restaurantTablesConnection.findAll(year, month, day, beginningHour, stayDuration);
    }

    public void addRestaurantTableToList(RestaurantTable restaurantTable) {
        this.restaurantTableList.add(restaurantTable);
        this.adapter.notifyDataSetChanged();
    }
}
