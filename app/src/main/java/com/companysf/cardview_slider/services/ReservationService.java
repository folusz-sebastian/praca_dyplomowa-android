package com.companysf.cardview_slider.services;

import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import com.companysf.cardview_slider.app.Restaurant;
import com.companysf.cardview_slider.connection.ReservationConnection;
import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ReservationService {
    private Reservation reservation;
    private ArrayAdapter<Integer> durationListSpinnerAdapter;
    private ArrayList<Integer> durations;
    private RestaurantTableService restaurantTableService;
    private ReservationConnection reservationConnection;

    public ReservationService(List<Reservation> reservationList,
                              ReservationConnection.ReservationsListener reservationsListener,
                              String dateFormat
                              ) {
        this.reservationConnection = new ReservationConnection(
                reservationList,
                reservationsListener,
                dateFormat
        );
    }

    public ReservationService(ReservationConnection.ReservationListener reservationListener, String dateFormat) {
        this.reservationConnection = new ReservationConnection(reservationListener, dateFormat);
    }

    public ReservationService(
            Reservation reservation,
            ArrayAdapter<Integer> durationListSpinnerAdapter,
            ArrayList<Integer> durations,
            RestaurantTablesConnection restaurantTablesConnection,
            List<RestaurantTable> restaurantTableList,
            RecyclerView.Adapter restaurantTableAdapter) {
        this.reservation = reservation;
        this.durationListSpinnerAdapter = durationListSpinnerAdapter;
        this.durations = durations;
        this.restaurantTableService = new RestaurantTableService(
                restaurantTablesConnection,
                restaurantTableList,
                restaurantTableAdapter
        );
    }

    private ArrayList<Integer> listStayingInRestaurantDuration() {
        int maxDuration =
                Restaurant.ENDING_JOB_HOUR - this.reservation.getBeginningHour();

        this.durations.clear();
        for (int i = 1; i < maxDuration + 1; i++) {
            this.durations.add(i);
        }
        return durations;
    }

    public void updateDurationList() {
        int oldDuration = this.reservation.getDuration();
        ArrayList<Integer> myDurations = listStayingInRestaurantDuration();
        int maxDuration = myDurations.get(myDurations.size() - 1);
        if (oldDuration > maxDuration) {
            this.reservation.setDuration(maxDuration);
        }
        this.durationListSpinnerAdapter.notifyDataSetChanged();
    }

    public void retrieveRestaurantTables(
            SimpleDateFormat dateFormatYear,
            SimpleDateFormat dateFormatMonth,
            SimpleDateFormat dateFormatDay) {

        String year = dateFormatYear.format(this.reservation.getDate());
        String month = dateFormatMonth.format(this.reservation.getDate());
        String day = dateFormatDay.format(this.reservation.getDate());

        this.restaurantTableService.retrieveFreeRestaurantTables(
                year, month, day,
                this.reservation.getBeginningHour(),
                this.reservation.getDuration()
        );
    }

    public void saveReservation(Reservation reservation) {
        this.reservationConnection.save(reservation);
    }

    public void retrieveMyReservations() {
        this.reservationConnection.findByUser();
    }
}
