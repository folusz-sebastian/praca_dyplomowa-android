package com.companysf.cardview_slider.services;

import android.support.v7.widget.RecyclerView;
import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;

import java.util.List;

public class RestaurantTableManagementService extends RestaurantTableService {
    private RestaurantTablesConnection restaurantTablesConnection;

    public RestaurantTableManagementService(
            List<RestaurantTable> restaurantTableList,
            RecyclerView.Adapter adapter,
            RestaurantTablesConnection restaurantTablesConnection
    ) {
        super(restaurantTablesConnection, restaurantTableList, adapter);
        this.restaurantTablesConnection = restaurantTablesConnection;
    }

    public void deleteRestaurantTable(long id) {
        restaurantTablesConnection.delete(id);
    }
}
