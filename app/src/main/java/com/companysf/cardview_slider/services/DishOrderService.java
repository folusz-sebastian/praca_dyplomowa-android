package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.DishOrderConnection;
import com.companysf.cardview_slider.entity.model.order.Realm.RealmDishOrder;
import com.companysf.cardview_slider.entity.model.order.postMethodBody.DishOnlyId;
import com.companysf.cardview_slider.entity.model.order.postMethodBody.DishOrder;
import com.companysf.cardview_slider.entity.model.order.postMethodBody.Order;
import com.companysf.cardview_slider.entity.model.order.postMethodBody.OrderStatusOnlyName;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class DishOrderService {
    private DishOrderConnection connection;

    public DishOrderService(DishOrderConnection.OrderListener listener) {
        connection = new DishOrderConnection(listener);
    }

    public static double countTotalPrice(List<RealmDishOrder> dishOrderList) {
        double totalPrice = 0.0;
        for (RealmDishOrder dishOrder : dishOrderList) {
            totalPrice += dishOrder.getQuantity() * dishOrder.getDish().getPrice();
        }
        return totalPrice;
    }

    public static int countOrderItems(List<RealmDishOrder> dishOrderList) {
        int orderItems = 0;
        for (RealmDishOrder dishOrder : dishOrderList) {
            orderItems += dishOrder.getQuantity();
        }
        return orderItems;
    }

    public static RealmResults<RealmDishOrder> findAllAsync() {
        return Realm.getDefaultInstance().where(RealmDishOrder.class).findAllAsync();
    }

    public static RealmResults<RealmDishOrder> findAll() {
        return Realm.getDefaultInstance().where(RealmDishOrder.class).findAll();
    }

    public static void addDishToOrder(com.companysf.cardview_slider.entity.model.dish.Dish dish) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            RealmDishOrder dishOrder = realm.where(RealmDishOrder.class).equalTo("dish.id", dish.getId()).findFirst();
            if (dishOrder == null) {
                dishOrder = new RealmDishOrder();
                dishOrder.setDish(dish);
                dishOrder.setQuantity(1);
            } else {
                dishOrder.setQuantity(dishOrder.getQuantity() + 1);
            }
            realm.copyToRealmOrUpdate(dishOrder);
        });
    }

    public static void removeItems() {
        Realm.getDefaultInstance().executeTransactionAsync(realm -> realm.delete(RealmDishOrder.class));
    }

    public static void removeItems(RealmDishOrder dishOrder) {
        Realm.getDefaultInstance().executeTransaction(realm -> realm.where(RealmDishOrder.class).equalTo("dish.id", dishOrder.getDish().getId()).findAll().deleteAllFromRealm());
    }

    public void save(List<RealmDishOrder> dishOrderItems, Reservation selectedReservation) {
        Order order = dishOrderItemsToOrder(dishOrderItems, selectedReservation);
        this.connection.save(order);
    }

    private Order dishOrderItemsToOrder(List<RealmDishOrder> dishOrderItems, Reservation selectedReservation) {
        Order order = new Order();
        OrderStatusOnlyName status = new OrderStatusOnlyName(AppConfig.STATUS_DISH__IS_BEING_PREPARED);
        order.setCost(countTotalPrice(dishOrderItems));
        order.setStatus(status);
        List<DishOrder> dishOrderList = new ArrayList<>();
        for (RealmDishOrder realmDishOrder : dishOrderItems) {
            DishOrder dishOrder = new DishOrder(
                    new DishOnlyId(realmDishOrder.getDish().getId()),
                    realmDishOrder.getQuantity()
            );
            dishOrderList.add(dishOrder);
        }
        order.setDishOrders(dishOrderList);
        order.setReservation(new Reservation(selectedReservation.getId()));
        return order;
    }
}
