package com.companysf.cardview_slider.services;

import android.content.ContentResolver;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.util.Objects;

public class ImageUploadService {
    private StorageReference mStorageRef;
    private UploadTask uploadTask;
    private ContentResolver contentResolver;
    private ImageUploadListener listener;

    public ImageUploadService(ContentResolver contentResolver, ImageUploadListener listener, String filesLocation) {
        this.contentResolver = contentResolver;
        this.listener = listener;
        this.mStorageRef = FirebaseStorage.getInstance().getReference(filesLocation);
    }

    public void uploadFile(Uri uri) {
        if (uri != null) {
            StorageReference storageReference = mStorageRef.child(System.currentTimeMillis() +
                    "." + getFileExtension(uri));
            uploadTask = storageReference.putFile(uri);
            uploadTask
                    .addOnSuccessListener(taskSnapshot -> {
                        uploadTask
                                .continueWithTask(task -> {
                                    if (!task.isSuccessful()) {
                                        throw Objects.requireNonNull(task.getException());
                                    }
                                    return storageReference.getDownloadUrl();
                                })
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful() && task.getResult() != null) {
                                        listener.onUploadImageSuccess(task.getResult().toString());
                                    }
                                });
                    })
                    .addOnFailureListener(exception -> {
                        listener.onUploadImageFailure();
                    });
        }
    }

    private String getFileExtension(Uri uri) {
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(this.contentResolver.getType(uri));
    }

    public interface ImageUploadListener {
        void onUploadImageSuccess(String imageUrl);
        void onUploadImageFailure();
    }

}
