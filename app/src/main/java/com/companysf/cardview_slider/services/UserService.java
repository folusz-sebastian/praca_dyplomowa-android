package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.connection.UserConnection;
import com.companysf.cardview_slider.entity.model.user.LoginForm;
import com.companysf.cardview_slider.entity.model.user.realm.LoginSessionRoleRealm;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.companysf.cardview_slider.entity.model.user.realm.UserRoleRealm;
import com.companysf.cardview_slider.utils.SessionManager;

import io.realm.Realm;
import io.realm.RealmList;

public class UserService {
    private UserConnection connection;

    public UserService(UserConnection.LoginListener listener, String dateFormat) {
        this.connection = new UserConnection(listener, dateFormat);
    }

    public UserService() {
        this.connection = new UserConnection();
    }

    public void retrieveUser(LoginForm loginForm) {
        this.connection.getUser(loginForm);
    }

    public static void saveInDevice(UserRealm user) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.delete(UserRealm.class);
            realm.copyToRealmOrUpdate(user);
        });
    }

    public static void saveInDevice(String authenticationKey) {
        SessionManager.getInstance().saveAuthKey(authenticationKey);
    }

    public static void saveLoginSessionUserRole(String role) {
        LoginSessionRoleRealm loginSessionRoleRealm = new LoginSessionRoleRealm(role);
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.delete(LoginSessionRoleRealm.class);
            realm.copyToRealm(loginSessionRoleRealm);
        });
    }

    public static String getAuthKey() {
        return SessionManager.getInstance().getAuthKey();
    }

    public static UserRealm getUserFromRealm() {
        return Realm.getDefaultInstance().where(UserRealm.class).findFirst();
    }

    public static boolean userLoggedInAs(String userRole) {
        LoginSessionRoleRealm loginSessionRoleRealm = Realm.getDefaultInstance().where(LoginSessionRoleRealm.class).equalTo("name", userRole).findFirst();
        return loginSessionRoleRealm != null;
    }

    public static void removeUserFromDevice() {
        SessionManager.getInstance().clearData();
        Realm.getDefaultInstance().executeTransaction(realm -> realm.delete(UserRealm.class));
    }

    public static boolean userHasRole(String userRole) {
        RealmList<UserRoleRealm> roles = getUserFromRealm().getRoles();
        UserRoleRealm role = roles.where().equalTo("role", userRole).findFirst();
        return role != null;
    }

    public void logoutUserFromBackend() {
        this.connection.logoutUser();
    }
}
