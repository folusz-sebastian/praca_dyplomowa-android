package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.OrderConnection;
import com.companysf.cardview_slider.entity.model.order.Order3;
import java.util.List;

public class OrderService {
    private OrderConnection connection;

    public OrderService(List<Order3> orderList, OrderConnection.OrdersListener ordersListener, String dateFormat) {
        this.connection = new OrderConnection(orderList, ordersListener, dateFormat);
    }

    public OrderService(Order3 order, OrderConnection.OrderListener orderListener, String dateFormat) {
        this.connection = new OrderConnection(order, orderListener, dateFormat);
    }

    public void retrieveOrdersNotPrepared() {
        this.connection.findAll();
    }

    public void retrieveOrderById(long orderId) {
        this.connection.findById(orderId);
    }

    public void changeOrderStatus(long orderId) {
        this.connection.updateOrderStatus(orderId, AppConfig.STATUS_DISH_IS_MADE);
    }
}
