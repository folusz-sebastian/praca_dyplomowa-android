package com.companysf.cardview_slider.services;

import android.support.v7.widget.RecyclerView;

import com.companysf.cardview_slider.connection.DishConnection;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;

import java.util.List;

public class DishService {
    private DishConnection connection;

    public DishService(List<DishIngredient> dishIngredientList, RecyclerView.Adapter adapter, DishConnection.DishListener dishListener) {
        this.connection = new DishConnection(dishIngredientList, adapter, dishListener);
    }

    public DishService(List<Dish> dishList, RecyclerView.Adapter adapter) {
        this.connection = new DishConnection(dishList, adapter);
    }

    public void retrieveDish(long id) {
        this.connection.getDishes(id);
    }
    public void retrieveDishes(String category) {this.connection.getDishes(category);}
}
