package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.connection.NewRestaurantTableConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;

public class NewRestaurantTableService {
    private NewRestaurantTableConnection connection;
    private RestaurantTable newRestaurantTable;

    public NewRestaurantTableService(RestaurantTable newRestaurantTable, NewRestaurantTableConnection.RestaurantTableListener restaurantTableListener) {
        this.newRestaurantTable = newRestaurantTable;
        this.connection = new NewRestaurantTableConnection(restaurantTableListener);
    }

    public void save() {
        connection.save(newRestaurantTable);
    }

    public void addChair() {
        this.newRestaurantTable.setChairs(this.newRestaurantTable.getChairs() + 1);
    }

    public void removeChair() {
        if (this.newRestaurantTable.getChairs() > 1) {
            this.newRestaurantTable.setChairs(this.newRestaurantTable.getChairs() - 1);
        }
    }

    public void settingTablePlaceName(String tablePlaceName) {
        newRestaurantTable.getPlace().setEmptyName(false);
        newRestaurantTable.getPlace().setName(tablePlaceName);
    }
}
