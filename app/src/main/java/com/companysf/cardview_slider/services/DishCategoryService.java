package com.companysf.cardview_slider.services;

import com.companysf.cardview_slider.connection.DishCategoryConnection;
import com.companysf.cardview_slider.entity.model.dish.DishCategory;

import java.util.List;

public class DishCategoryService {
    private DishCategoryConnection connection;

    public DishCategoryService(List<DishCategory> dishCategoryList,
                               DishCategoryConnection.DishCategoryListener listener) {
        this.connection = new DishCategoryConnection(dishCategoryList, listener);
    }

    public void retrieveDishCategories() {
        this.connection.getDishCategories();
    }
}
