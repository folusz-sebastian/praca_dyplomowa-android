package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.order.DishOrder2;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class LastTransactionsAdapter extends RecyclerView.Adapter<LastTransactionsAdapter.MyViewHolder> {
    private Context context;
    private List<DishOrder2> lastTransactionList;

    public LastTransactionsAdapter(List<DishOrder2> lastTransactionList) {
        this.lastTransactionList = lastTransactionList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, time;
        ImageView dishImage;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            dishImage = itemView.findViewById(R.id.dishImage);
            context = itemView.getContext();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.last_transactions_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        DishOrder2 dishOrder = lastTransactionList.get(position);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                context.getString(R.string.date_format_PL),
                new Locale(context.getString(R.string.polish), context.getString(R.string.poland))
        );
        SimpleDateFormat timeFormat = new SimpleDateFormat(
                context.getString(R.string.time_format),
                new Locale(context.getString(R.string.polish), context.getString(R.string.poland))
        );

        myViewHolder.title.setText(dishOrder.getDish().getName());
        myViewHolder.date.setText(
                dateFormat.format(dishOrder.getOrder().getDateTime())
        );
        myViewHolder.time.setText(
                timeFormat.format(dishOrder.getOrder().getDateTime())
        );
        Picasso.get().load(dishOrder.getDish().getImageUrl()).into(myViewHolder.dishImage);
    }

    @Override
    public int getItemCount() {
        return lastTransactionList.size();
    }
}
