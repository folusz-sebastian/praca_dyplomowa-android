package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.services.RestaurantTableManagementService;

import java.util.List;

public class RestaurantTablesManagementAdapter extends RecyclerView.Adapter<RestaurantTablesManagementAdapter.MyViewHolder> {
    private List<RestaurantTable> restaurantTablesList;
    private RestaurantTableManagementService service;
    private Context context;

    public RestaurantTablesManagementAdapter(List<RestaurantTable> restaurantTablesList) {
        this.restaurantTablesList = restaurantTablesList;
        this.service = new RestaurantTableManagementService(
                restaurantTablesList,
                this,
                new RestaurantTablesConnection(restaurantTablesList, this)
        );
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, tablePlace, numberOfChairs, removeBtn;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            tablePlace = itemView.findViewById(R.id.tablePlace);
            numberOfChairs = itemView.findViewById(R.id.chairs);
            removeBtn = itemView.findViewById(R.id.removeBtn);
            context = itemView.getContext();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.restaurant_table_management_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        RestaurantTable restaurantTable = restaurantTablesList.get(position);

        myViewHolder.title.setText(
                context.getString(R.string.restaurant_table_id_info, restaurantTable.getId())
        );
        myViewHolder.numberOfChairs.setText(
                context.getString(R.string.number_of_chairs_with_text, restaurantTable.getChairs())
        );
        myViewHolder.tablePlace.setText(restaurantTable.getPlace().getName());
        myViewHolder.removeBtn.setOnClickListener(
                v -> service.deleteRestaurantTable(restaurantTable.getId())
        );
    }

    @Override
    public int getItemCount() {
        return restaurantTablesList.size();
    }
}
