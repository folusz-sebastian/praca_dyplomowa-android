package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.companysf.cardview_slider.Activity.OrderDetailsDialog;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.order.Order3;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class OrdersNotPreparedAdapter extends RecyclerView.Adapter<OrdersNotPreparedAdapter.MyViewHolder> {
    private List<Order3> orderList;
    private Context context;

    public OrdersNotPreparedAdapter(List<Order3> orderList) {
        this.orderList = orderList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView detailsBtn;
        private TextView reservationDate, beginningHour, tableInfo;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            detailsBtn = itemView.findViewById(R.id.detailsBtn);
            reservationDate = itemView.findViewById(R.id.reservationDate);
            beginningHour = itemView.findViewById(R.id.beginningHour);
            tableInfo = itemView.findViewById(R.id.tableInfo);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orders_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Order3 order = orderList.get(position);
        Reservation reservation = order.getReservation();

        long restaurantTableId = reservation.getTable().getId();
        String restaurantTableLocation = reservation.getTable().getPlace().getName();
        String tableInfoText = this.context.getString(
                R.string.restaurant_table_info, restaurantTableId, restaurantTableLocation
        );

        myViewHolder.tableInfo.setText(
                tableInfoText
        );
        myViewHolder.reservationDate.setText(
                new SimpleDateFormat(
                        context.getString(R.string.date_format_PL),
                        new Locale(context.getString(R.string.polish), context.getString(R.string.poland))
                ).format(reservation.getDate())
        );
        myViewHolder.beginningHour.setText(
                context.getString(R.string.hour, reservation.getBeginningHour())
        );

        myViewHolder.detailsBtn.setOnClickListener(v -> showReservationDetailsDialog(order.getId()));
    }

    private void showReservationDetailsDialog(long orderId) {
        FragmentManager fm = ((FragmentActivity) this.context).getSupportFragmentManager();
        OrderDetailsDialog dialog = OrderDetailsDialog.newInstance(orderId);
        dialog.show(fm, context.getString(R.string.dialog_tag_order_details));
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
