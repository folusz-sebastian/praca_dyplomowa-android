package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class MyReservationsAdapter extends RecyclerView.Adapter<MyReservationsAdapter.MyViewHolder> {
    private List<Reservation> reservationList;
    private Context context;

    public MyReservationsAdapter(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView reservationDate, tablePlace, numberOfChairs, beginningHourAndDuration;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            reservationDate = itemView.findViewById(R.id.reservationDate);
            beginningHourAndDuration = itemView.findViewById(R.id.beginningHourAndDuration);
            tablePlace = itemView.findViewById(R.id.tablePlace);
            numberOfChairs = itemView.findViewById(R.id.chairs);
            context = itemView.getContext();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.my_reservations_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Reservation reservation = reservationList.get(position);

        Locale localePoland = new Locale(context.getString(R.string.polish), context.getString(R.string.poland));
        myViewHolder.reservationDate.setText(
                new SimpleDateFormat(
                        context.getString(R.string.date_format_PL),
                        localePoland
                ).format(reservation.getDate())
        );
        myViewHolder.beginningHourAndDuration.setText(
                context.getString(R.string.beginning_hour_duration,
                        reservation.getBeginningHour(), reservation.getDuration()
                )
        );
        myViewHolder.numberOfChairs.setText(
                context.getString(R.string.number_of_chairs_with_text, reservation.getTable().getChairs())
        );
        myViewHolder.tablePlace.setText(reservation.getTable().getPlace().getName());
    }

    @Override
    public int getItemCount() {
        return reservationList.size();
    }
}
