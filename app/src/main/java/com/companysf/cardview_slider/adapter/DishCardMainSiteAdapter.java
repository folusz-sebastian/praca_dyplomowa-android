package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.companysf.cardview_slider.Activity.DishActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DishCardMainSiteAdapter extends PagerAdapter {
    private List<Dish> dishList;
    private List<CardView> views;
    private float mBaseElevation;
    private Context context;
    public static final int MAX_ELEVATION = 5;

    public DishCardMainSiteAdapter() {
        this.dishList = new ArrayList<>();
        this.views = new ArrayList<>();
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    public void addCardItems(List<Dish> dishList) {
        for (int i = 0; i < dishList.size(); i++) {
            views.add(null);
        }
        this.dishList.addAll(dishList);
    }

    public CardView getCardViewAt(int position) {
        return views.get(position);
    }

    @Override
    public int getCount() {
        return dishList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.dish_card_main_site, container, false);
        this.context = view.getContext();
        container.addView(view);

        CardView cardView = view.findViewById(R.id.cardView);
        TextView titleTextView = view.findViewById(R.id.titleTextView);
        TextView descriptionTextView = view.findViewById(R.id.descriptionTextView);
        TextView dishPriceTextView = view.findViewById(R.id.dishPriceTextView);
        ImageView picture = view.findViewById(R.id.picture);
        ConstraintLayout dishCard = view.findViewById(R.id.dishCard);

        Dish dish = dishList.get(position);

        titleTextView.setText(dish.getName());
        descriptionTextView.setText(
                dish.getDescription()
        );
        dishPriceTextView.setText(
                context.getString(R.string.price_with_currency, dish.getPrice())
        );

        Picasso
                .get()
                .load(dish.getImageUrl())
                .resize(750, 600)
                .centerCrop()
                .into(picture);

        dishCard.setOnClickListener(v -> switchToDishActivity(dish.getId()));

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION);

        views.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        views.set(position, null);
    }

    private void switchToDishActivity(long dishId) {
        Intent intent = new Intent(context, DishActivity.class);
        intent.putExtra(context.getString(R.string.intent_extra_dish_id), dishId);
        context.startActivity(intent);
    }
}
