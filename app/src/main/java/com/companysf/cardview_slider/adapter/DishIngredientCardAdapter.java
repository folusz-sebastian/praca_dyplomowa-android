package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;

import java.util.List;

public class DishIngredientCardAdapter extends RecyclerView.Adapter<DishIngredientCardAdapter.MyViewHolder> {
    private Context context;
    private List<DishIngredient> dishIngredientList;
    private boolean showDeleteIcon;

    public DishIngredientCardAdapter(List<DishIngredient> dishIngredientList, boolean showDeleteIcon) {
        this.dishIngredientList = dishIngredientList;
        this.showDeleteIcon = showDeleteIcon;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dish_ingredient_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        DishIngredient dishIngredient = dishIngredientList.get(position);

        myViewHolder.ingredientName.setText(dishIngredient.getIngredient().getName());
        myViewHolder.ingredientMeasurementUnit.setText(
                dishIngredient.getIngredient().getMeasurementUnit().getName()
        );
        myViewHolder.ingredientQuantity.setText(
                context.getString(R.string.ingredient_quantity_value, dishIngredient.getQuantity())
        );

        myViewHolder.deleteIcon.setOnClickListener(v -> removeIngredient(position));
        if (!showDeleteIcon) {
            myViewHolder.deleteIcon.setVisibility(View.GONE);
        }
    }

    private void removeIngredient(int position) {
        dishIngredientList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dishIngredientList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ingredientName, ingredientQuantity, ingredientMeasurementUnit;
        ImageView deleteIcon;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ingredientName = itemView.findViewById(R.id.ingredientName);
            ingredientQuantity = itemView.findViewById(R.id.ingredientQuantity);
            ingredientMeasurementUnit = itemView.findViewById(R.id.ingredientMeasurementUnit);
            deleteIcon = itemView.findViewById(R.id.deleteIcon);
            context = itemView.getContext();
        }
    }
}
