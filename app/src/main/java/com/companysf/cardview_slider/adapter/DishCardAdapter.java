package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.companysf.cardview_slider.Activity.DishActivity;
import com.companysf.cardview_slider.Activity.RestaurantMenuActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DishCardAdapter extends RecyclerView.Adapter<DishCardAdapter.MyViewHolder> {
    private Context context;
    private List<Dish> dishList;

    public DishCardAdapter(List<Dish> dishList) {
        this.dishList = dishList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dish_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Dish dish = dishList.get(position);

        myViewHolder.titleTextView.setText(dish.getName());
        myViewHolder.descriptionTextView.setText(
                dish.getDescription()
        );
        myViewHolder.dishPriceTextView.setText(
                context.getString(R.string.price_with_currency, dish.getPrice())
        );

        Picasso.get().load(dish.getImageUrl()).fit().centerCrop().into(myViewHolder.dishImage);

        myViewHolder.dishCard.setOnClickListener(view -> switchToDishActivity(dish.getId()));
    }

    @Override
    public int getItemCount() {
        return dishList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView, descriptionTextView, dishPriceTextView;
        ImageView dishImage;
        ConstraintLayout dishCard = itemView.findViewById(R.id.dishCard);

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            dishPriceTextView = itemView.findViewById(R.id.dishPriceTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            dishImage = itemView.findViewById(R.id.picture);
            context = itemView.getContext();
        }
    }

    private void switchToDishActivity(long dishId) {
        Intent intent = new Intent(context, DishActivity.class);
        intent.putExtra(context.getString(R.string.intent_extra_dish_id), dishId);
        context.startActivity(intent);
    }
}
