package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.connection.ReservationConnection;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.services.ReservationService;

import java.util.List;

public class RestaurantTablesInReservationAdapter extends RecyclerView.Adapter<RestaurantTablesInReservationAdapter.MyViewHolder> {
    private List<RestaurantTable> restaurantTablesList;
    private Reservation reservation;
    private ReservationService reservationService;
    private Context context;
    private ReservationConnection.ReservationListener reservationListener;

    public RestaurantTablesInReservationAdapter(
            List<RestaurantTable> restaurantTablesList,
            Reservation reservation,
            ReservationConnection.ReservationListener reservationListener) {
        this.restaurantTablesList = restaurantTablesList;
        this.reservation = reservation;
        this.reservationListener = reservationListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, tablePlace, numberOfChairs, reserveBtn;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            tablePlace = itemView.findViewById(R.id.tablePlace);
            numberOfChairs = itemView.findViewById(R.id.chairs);
            reserveBtn = itemView.findViewById(R.id.reserveBtn);
            context = itemView.getContext();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.restaurant_table_in_reservation_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        this.reservationService = new ReservationService(
                reservationListener,
                context.getString(R.string.date_format_US)
        );

        RestaurantTable restaurantTable = restaurantTablesList.get(position);

        myViewHolder.title.setText(
                context.getString(R.string.restaurant_table_id_info, restaurantTable.getId())
        );
        myViewHolder.numberOfChairs.setText(
                context.getString(R.string.number_of_chairs_with_text, restaurantTable.getChairs())
        );
        myViewHolder.tablePlace.setText(restaurantTable.getPlace().getName());
        myViewHolder.reserveBtn.setOnClickListener(view -> {
            reservation.setTable(restaurantTablesList.get(position));
            reservationService.saveReservation(reservation);
        });
    }

    @Override
    public int getItemCount() {
        return restaurantTablesList.size();
    }
}
