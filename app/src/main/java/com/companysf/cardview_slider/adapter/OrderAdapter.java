package com.companysf.cardview_slider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.order.Realm.RealmDishOrder;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    private Context context;
    private List<RealmDishOrder> dishOrders = Collections.emptyList();
    private OrderCardListener listener;

    public OrderAdapter(OrderCardListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dish_order_card, viewGroup, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        RealmDishOrder dishOrder = dishOrders.get(position);
        myViewHolder.dishPrice.setText(
                this.context.getString(
                        R.string.price_with_currency,
                        dishOrder.getDish().getPrice()
                )
        );
        myViewHolder.dishName.setText(dishOrder.getDish().getName());
        myViewHolder.dishQuantity.setText(
                this.context.getString(R.string.dish_quantity, dishOrder.getQuantity())
        );
        Picasso.get().load(dishOrder.getDish().getImageUrl()).into(myViewHolder.dishImage);
        myViewHolder.removeDish.setOnClickListener(v -> listener.onRemoveDish(dishOrder));
    }

    @Override
    public int getItemCount() {
        return dishOrders.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dishPrice, dishName, dishQuantity;
        ImageView dishImage, removeDish;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            dishName = itemView.findViewById(R.id.name);
            dishPrice = itemView.findViewById(R.id.priceAndQuantity);
            dishImage = itemView.findViewById(R.id.dishImage);
            dishQuantity = itemView.findViewById(R.id.quantity);
            removeDish = itemView.findViewById(R.id.removeDish);
        }
    }

    public void setData(List<RealmDishOrder> dishOrders) {
        this.dishOrders = dishOrders;
        notifyDataSetChanged();
    }

    public interface OrderCardListener {
        void onRemoveDish(RealmDishOrder dishOrder);
    }
}
