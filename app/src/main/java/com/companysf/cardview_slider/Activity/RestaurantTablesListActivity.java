package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.RestaurantTablesManagementAdapter;
import com.companysf.cardview_slider.connection.NewRestaurantTableConnection;
import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.services.RestaurantTableManagementService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.Toolbar;

import java.util.ArrayList;

public class RestaurantTablesListActivity extends AppCompatActivity implements NewRestaurantTableConnection.RestaurantTableListener {
    private RestaurantTablesManagementAdapter adapter;
    private RestaurantTableManagementService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_table_management_list);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        Button removeBtn = findViewById(R.id.removeBtn);

        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);
        toolbarWrap.setToolbar(this);
        toolbarWrap.enableToolbarUpNavigation(this);

        removeBtn.setOnClickListener(view -> showDialog());

        RecyclerView tablesListRecyclerView = findViewById(R.id.restaurantTablesListRecyclerView);
        ArrayList<RestaurantTable> restaurantTableList = new ArrayList<>();
        this.adapter = new RestaurantTablesManagementAdapter(restaurantTableList);
        this.service = new RestaurantTableManagementService(
                restaurantTableList,
                this.adapter,
                new RestaurantTablesConnection(restaurantTableList, this.adapter)
        );

        setRecyclerView(tablesListRecyclerView);

        this.service.retrieveRestaurantTables();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRecyclerView(RecyclerView tablesListRecyclerView) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        tablesListRecyclerView.setLayoutManager(layoutManager);
        tablesListRecyclerView.setNestedScrollingEnabled(false);
        tablesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tablesListRecyclerView.setAdapter(this.adapter);
    }

    private void showDialog() {
        FragmentManager fm = getSupportFragmentManager();
        RestaurantTablesListDialog dialog = RestaurantTablesListDialog.newInstance();
        dialog.show(fm, getString(R.string.dialog_tag_restaurant_tableList));
    }

    @Override
    public void onGettingRestaurantTable(RestaurantTable body) {
        service.addRestaurantTableToList(body);
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
