package com.companysf.cardview_slider.Activity.mainActivity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.companysf.cardview_slider.Activity.LastTransactionsActivity;
import com.companysf.cardview_slider.Activity.LoginActivity;
import com.companysf.cardview_slider.Activity.MyReservationsActivity;
import com.companysf.cardview_slider.Activity.ReservationActivity;
import com.companysf.cardview_slider.Activity.RestaurantMenuActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.LastTransactionsAdapter;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.LastTransactionsConnection;
import com.companysf.cardview_slider.entity.model.order.DishOrder2;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.utils.PageTransformer;
import com.companysf.cardview_slider.adapter.DishCardMainSiteAdapter;
import com.companysf.cardview_slider.connection.DishConnection;
import com.companysf.cardview_slider.views.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class CustomerMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customer);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        } else {
            UserService.saveLoginSessionUserRole(AppConfig.USER_ROLE_CUSTOMER);

            ViewPager mViewPager = findViewById(R.id.viewPager);
            ImageView pizzaBtn = findViewById(R.id.pizzaCategory);
            ImageView dinnerBtn = findViewById(R.id.dinnerCategory);
            ImageView dessertBtn = findViewById(R.id.dessertCategory);
            ImageView fishBtn = findViewById(R.id.fishCategory);
            ImageView soupBtn = findViewById(R.id.soupCategory);
            RecyclerView transactionsRecyclerView = findViewById(R.id.restaurantTablesList);
            TextView lastTransactionsBtn = findViewById(R.id.lastTransactionsBtn);
            TextView menuBtn = findViewById(R.id.menuBtn);

            Button tablesToBookBtn = findViewById(R.id.tablesToBookBtn);
            Button myReservationsBtn = findViewById(R.id.myReservationsBtn);
            Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

            toolbarWrap.setToolbar(this);

            List<DishOrder2> lastTransactionList = new ArrayList<>();

            DishCardMainSiteAdapter dishCardMainSiteAdapter = new DishCardMainSiteAdapter();
            mViewPager.setAdapter(dishCardMainSiteAdapter);
            DishConnection dishConnection = new DishConnection(dishCardMainSiteAdapter);

            LastTransactionsAdapter lastTransactionsAdapter = new LastTransactionsAdapter(lastTransactionList);
            LastTransactionsConnection lastTransactionsConnection = new LastTransactionsConnection(
                    getString(R.string.date_format_with_hour),
                    lastTransactionList,
                    lastTransactionsAdapter
            );

            setRecyclerView(transactionsRecyclerView, lastTransactionsAdapter);

            PageTransformer pageTransformer = new PageTransformer(mViewPager, dishCardMainSiteAdapter);
            mViewPager.setPageTransformer(false, pageTransformer);
            mViewPager.setOffscreenPageLimit(3);

            dishConnection.getDishes(false, null);
            lastTransactionsConnection.getLastTransactions(false);

            pizzaBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_PIZZA));
            dinnerBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_DINNER));
            dessertBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_DESSERT));
            fishBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_FISH));
            soupBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_SOUP));

            lastTransactionsBtn.setOnClickListener(view -> switchToLastTransactions());
            menuBtn.setOnClickListener(view -> switchToMenu());
            tablesToBookBtn.setOnClickListener(view -> switchToChoosingReservation());
            myReservationsBtn.setOnClickListener(view -> switchToMyReservations());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuLogOutIcon) {
            switchToLoginActivity();
            UserService.removeUserFromDevice();
            new UserService().logoutUserFromBackend();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerView(RecyclerView transactionsRecyclerView,
                                 LastTransactionsAdapter lastTransactionsAdapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        transactionsRecyclerView.setLayoutManager(layoutManager);
        transactionsRecyclerView.setNestedScrollingEnabled(false);
        transactionsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        transactionsRecyclerView.setAdapter(lastTransactionsAdapter);
        transactionsRecyclerView.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        );
    }

    private void switchToChoosingReservation() {
        Intent intent = new Intent(this, ReservationActivity.class);
        startActivity(intent);
    }

    private void switchToMyReservations() {
        Intent intent = new Intent(this, MyReservationsActivity.class);
        startActivity(intent);
    }

    private void switchToMenu() {
        switchToMenu(null);
    }

    private void switchToMenu(String category) {
        Intent intent = new Intent(this, RestaurantMenuActivity.class);
        intent.putExtra(getString(R.string.intent_extra_category), category);
        startActivity(intent);
    }

    private void switchToLastTransactions() {
        Intent intent = new Intent(this, LastTransactionsActivity.class);
        startActivity(intent);
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
