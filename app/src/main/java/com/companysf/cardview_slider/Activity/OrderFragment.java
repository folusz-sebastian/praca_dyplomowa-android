package com.companysf.cardview_slider.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.OrderAdapter;
import com.companysf.cardview_slider.connection.DishOrderConnection;
import com.companysf.cardview_slider.connection.ReservationConnection;
import com.companysf.cardview_slider.entity.model.order.Realm.RealmDishOrder;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.services.DishOrderService;
import com.companysf.cardview_slider.services.ReservationService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;
import lombok.NoArgsConstructor;

import static solid.collectors.ToSolidList.toSolidList;
import static solid.stream.Stream.stream;

@NoArgsConstructor
public class OrderFragment extends BottomSheetDialogFragment implements OrderAdapter.OrderCardListener, DishOrderConnection.OrderListener, ReservationConnection.ReservationsListener {
    private Context context;
    private Button btnCheckout;
    private Spinner myReservationSpinner;
    private ImageView closeBtn;
    private TextView removeAll;
    private RecyclerView recyclerView;
    private OrderAdapter adapter;
    private RealmResults<RealmDishOrder> dishOrderRealmResults;
    private OrderedRealmCollectionChangeListener<RealmResults<RealmDishOrder>> orderItemsChangeListener;
    private DishOrderService dishOrderService;
    private List<Reservation> myReservationList;
    private LinearLayout emptyReservationListText;
    private TextView reserveTableBtn;
    private Reservation selectedReservation;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return bottomSheetToFullHeight(
                (BottomSheetDialog) super.onCreateDialog(savedInstanceState)
        );
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders_bottom_sheet, container, false);

        this.context = view.getContext();
        this.recyclerView = view.findViewById(R.id.recyclerView);
        this.btnCheckout = view.findViewById(R.id.checkoutOrder);
        this.closeBtn = view.findViewById(R.id.closeBtn);
        this.removeAll = view.findViewById(R.id.removeAll);
        this.myReservationSpinner = view.findViewById(R.id.myReservationSpinner);
        this.emptyReservationListText = view.findViewById(R.id.emptyReservationListText);
        this.reserveTableBtn = view.findViewById(R.id.reserveTableBtn);

        setRecyclerView();
        this.adapter = new OrderAdapter(this);
        this.recyclerView.setAdapter(this.adapter);
        this.dishOrderService = new DishOrderService(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dishOrderRealmResults = DishOrderService.findAllAsync();
        adapter.setData(dishOrderRealmResults);

        this.myReservationList = new ArrayList<>();
        ReservationService reservationService =
                new ReservationService(
                        this.myReservationList,
                        this,
                        context.getString(R.string.date_format_US)
                );
        reservationService.retrieveMyReservations();

        btnCheckout.setText(
                this.context.getString(R.string.order_cost_for_order_btn, getOrderTotalPrice(dishOrderRealmResults))
        );

        orderItemsChangeListener = (orderItems, changeSet) -> {
            adapter.setData(orderItems);
            btnCheckout.setText(
                    this.context.getString(R.string.order_cost_for_order_btn, getOrderTotalPrice(orderItems))
            );
            if (orderItems.size() == 0 || this.myReservationList.isEmpty()) {
                this.btnCheckout.setEnabled(false);
            } else {
                this.btnCheckout.setEnabled(true);
            }

        };
        dishOrderRealmResults.addChangeListener(orderItemsChangeListener);

        this.btnCheckout.setOnClickListener(v -> {
            dishOrderService.save(DishOrderService.findAllAsync(), selectedReservation);
            this.btnCheckout.setEnabled(false);
        });
        this.closeBtn.setOnClickListener(v -> dismiss());
        this.removeAll.setOnClickListener(v -> DishOrderService.removeItems());
        this.reserveTableBtn.setOnClickListener(v -> switchToReservationActivity());

        this.myReservationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0 && !DishOrderService.findAll().isEmpty()) {
                    selectedReservation = myReservationList.get(position-1);
                    btnCheckout.setEnabled(true);
                } else {
                    btnCheckout.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(context, context.getString(R.string.error_on_spinner_nothing_selected), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void switchToReservationActivity() {
        Intent intent = new Intent(context, ReservationActivity.class);
        dismiss();
        startActivity(intent);
    }

    private void initMyReservationSpinner() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                context.getString(R.string.date_format_PL),
                new Locale(context.getString(R.string.polish), context.getString(R.string.poland))
        );

        List<String> myReservationListStrings = stream(myReservationList).map(reservation -> {
            String reservationDate = dateFormat.format(reservation.getDate());
            return context.getString(R.string.reservation_date_time_duration,
                    reservationDate, reservation.getBeginningHour(), reservation.getDuration()
            );
        }).collect(toSolidList());
        LinkedList<String> spinnerValues = new LinkedList<>(myReservationListStrings);

        if (spinnerValues.isEmpty()) {
            this.myReservationSpinner.setVisibility(View.GONE);
            this.emptyReservationListText.setVisibility(View.VISIBLE);
        } else {
            spinnerValues.addFirst(context.getString(R.string.reservation_date_time_duration_default));
        }

        ArrayAdapter<String> myReservationSpinnerAdapter = new ArrayAdapter<>(this.context, android.R.layout.simple_spinner_item, spinnerValues);
        myReservationSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.myReservationSpinner.setAdapter(myReservationSpinnerAdapter);
    }

    private String getOrderTotalPrice(List<RealmDishOrder> dishOrderList) {
        return this.context.getString(R.string.price_with_currency, DishOrderService.countTotalPrice(dishOrderList));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (dishOrderRealmResults != null) {
            dishOrderRealmResults.removeChangeListener(orderItemsChangeListener);
        }
    }

    private BottomSheetDialog bottomSheetToFullHeight(BottomSheetDialog dialog) {
        dialog.setOnShowListener(dialog1 -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog1;

            FrameLayout bottomSheet = d.findViewById(android.support.design.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        return dialog;
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.context);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setNestedScrollingEnabled(false);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.recyclerView.addItemDecoration(
                new DividerItemDecoration(Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL)
        );
    }

    @Override
    public void onRemoveDish(RealmDishOrder dishOrder) {
        DishOrderService.removeItems(dishOrder);
    }

    @Override
    public void onSuccessSavingOrder() {
        dismiss();
        DishOrderService.removeItems();
        Toast.makeText(this.getContext(), context.getString(R.string.success_on_saving_order), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorSavingOrder() {
        this.btnCheckout.setEnabled(true);
        Toast.makeText(this.getContext(), context.getString(R.string.error_on_saving_order), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGettingReservations() {
        initMyReservationSpinner();
    }
}
