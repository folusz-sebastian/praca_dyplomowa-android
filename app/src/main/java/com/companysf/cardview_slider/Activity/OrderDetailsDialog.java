package com.companysf.cardview_slider.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.OrderAdapter2;
import com.companysf.cardview_slider.connection.OrderConnection;
import com.companysf.cardview_slider.entity.model.order.Order3;
import com.companysf.cardview_slider.services.OrderService;

import java.util.Objects;

public class OrderDetailsDialog extends DialogFragment implements OrderConnection.OrderListener {
    private static final String ARG_PARAM1 = "dish_id";

    private long orderId;
    private OrderDetailsDialogListener listener;
    private OrderService service;
    private OrderAdapter2 adapter;

    private RecyclerView recyclerView;
    private Context context;
    private Button markAsDoneBtn;

    public OrderDetailsDialog() {
        // Required empty public constructor
    }

    public static OrderDetailsDialog newInstance(long dishId) {
        OrderDetailsDialog frag = new OrderDetailsDialog();
        frag.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);

        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, dishId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OrderDetailsDialogListener) {
            listener = (OrderDetailsDialogListener) context;
        } else {
            throw new RuntimeException(
                    getString(R.string.exception_on_not_implementing_listener2, context.toString())
            );
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderId = getArguments().getLong(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_details, container, false);
        this.context = view.getContext();

        getDialog().setTitle(this.context.getString(R.string.order_details_dialog_title, orderId));
        this.recyclerView = view.findViewById(R.id.recyclerView);
        this.markAsDoneBtn = view.findViewById(R.id.markAsDoneBtn);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setRecyclerView();
        Order3 order = new Order3();
        this.service = new OrderService(
                order,
                this,
                context.getString(R.string.date_format_US)
        );
        this.adapter = new OrderAdapter2();
        this.recyclerView.setAdapter(this.adapter);

        this.service.retrieveOrderById(orderId);
        this.markAsDoneBtn.setOnClickListener(view -> markOrderAsDone(this.orderId));
    }

    private void markOrderAsDone(long orderId) {
        this.markAsDoneBtn.setEnabled(false);
        service.changeOrderStatus(orderId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onGettingOrder(Order3 order) {
        this.adapter.setData(order.getDishOrders());
    }

    @Override
    public void onChangingOrderStatus() {
        getDialog().dismiss();
        if (listener != null) {
            listener.onMarkingOrderAsDone(orderId);
        }
    }

    public interface OrderDetailsDialogListener {
        void onMarkingOrderAsDone(long orderId);
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.context);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setNestedScrollingEnabled(false);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.recyclerView.addItemDecoration(
                new DividerItemDecoration(Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL)
        );
    }
}
