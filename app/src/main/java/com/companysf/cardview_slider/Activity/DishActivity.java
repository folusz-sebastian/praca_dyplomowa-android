package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.DishIngredientCardAdapter;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.DishConnection;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;
import com.companysf.cardview_slider.entity.model.order.Realm.RealmDishOrder;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.companysf.cardview_slider.services.DishService;
import com.companysf.cardview_slider.services.DishOrderService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.OrdersInfoBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;


public class DishActivity extends AppCompatActivity implements DishConnection.DishListener {
    private TextView titleTextView, descriptionTextView, dishPriceTextView;
    private ImageView dishPicture;
    private OrdersInfoBar ordersInfoBar;
    private Dish dish;
    private RealmResults<RealmDishOrder> dishOrderRealmResults;
    private OrderedRealmCollectionChangeListener<RealmResults<RealmDishOrder>> orderItemsChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish);

        UserRealm user = UserService.getUserFromRealm();
        if (user == null) {
            switchToLoginActivity();
        } else {
            RecyclerView recyclerView = findViewById(R.id.dishIngredientsRecyclerView);
            titleTextView = findViewById(R.id.titleTextView);
            descriptionTextView = findViewById(R.id.descriptionTextView);
            dishPriceTextView = findViewById(R.id.dishPrice);
            dishPicture = findViewById(R.id.picture);
            ordersInfoBar = findViewById(R.id.orderInfoBar);
            Button addToBasketBtn = findViewById(R.id.addToBasket);
            ImageView backBtn = findViewById(R.id.backBtn);

            dishOrderRealmResults = DishOrderService.findAllAsync();
            orderItemsChangeListener = (orderItems, changeSet) -> ordersInfoBar.setData(
                    DishOrderService.countOrderItems(orderItems),
                    DishOrderService.countTotalPrice(orderItems)
            );
            dishOrderRealmResults.addChangeListener(orderItemsChangeListener);

            List<DishIngredient> dishIngredientList = new ArrayList<>();
            DishIngredientCardAdapter adapter = new DishIngredientCardAdapter(dishIngredientList, false);

            setRecyclerView(recyclerView, adapter);

            DishService dishService = new DishService(dishIngredientList, adapter, this);
            long dishId = getIntent().getLongExtra(getString(R.string.intent_extra_dish_id), 0);
            dishService.retrieveDish(dishId);

            ordersInfoBar.setOnClickListener(v -> showCartItemsFragment());
            addToBasketBtn.setOnClickListener(v -> addDishToLocalDB());
            backBtn.setOnClickListener(v -> finish());

            if (UserService.userLoggedInAs(AppConfig.USER_ROLE_EMPLOYEE)) {
                ordersInfoBar.setVisibility(View.GONE);
                addToBasketBtn.setVisibility(View.GONE);
            }
        }
    }

    private void addDishToLocalDB() {
        if (this.dish != null) {
            Toast.makeText(this, "" + this.dish.getName(), Toast.LENGTH_LONG).show();
            DishOrderService.addDishToOrder(this.dish);
        }
    }

    private void showCartItemsFragment() {
        OrderFragment fragment = new OrderFragment();
        fragment.show(getSupportFragmentManager(), fragment.getTag());
    }

    private void setRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onGettingDish(Dish dish) {
        removeBackground(titleTextView);
        removeBackground(dishPriceTextView);
        removeBackground(descriptionTextView);

        setWidthToWrapContent(titleTextView);
        setWidthToWrapContent(dishPriceTextView);

        this.dish = dish;
        this.titleTextView.setText(dish.getName());
        this.descriptionTextView.setText(dish.getDescription());
        this.dishPriceTextView.setText(
                getString(R.string.price_with_currency, dish.getPrice())
        );
        Picasso.get()
                .load(dish.getImageUrl())
                .fit()
                .centerCrop()
                .into(this.dishPicture);
    }

    private void removeBackground(TextView textView) {
        textView.setBackgroundColor(Color.TRANSPARENT);
    }

    private void setWidthToWrapContent(TextView textView) {
        textView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        textView.requestLayout();
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (dishOrderRealmResults != null) {
            dishOrderRealmResults.removeChangeListener(orderItemsChangeListener);
        }
    }
}
