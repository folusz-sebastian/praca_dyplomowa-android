package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.OrdersNotPreparedAdapter;
import com.companysf.cardview_slider.connection.OrderConnection;
import com.companysf.cardview_slider.entity.model.order.Order3;
import com.companysf.cardview_slider.services.OrderService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class OrdersNotPreparedActivity extends AppCompatActivity implements OrderConnection.OrdersListener, OrderDetailsDialog.OrderDetailsDialogListener {

    private RecyclerView.Adapter adapter;
    private OrderService orderService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_not_prepared);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        RecyclerView recyclerView = findViewById(R.id.ordersRecyclerView);
        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

        toolbarWrap.setToolbar(this);
        toolbarWrap.enableToolbarUpNavigation(this);

        List<Order3> orderList = new ArrayList<>();
        this.adapter = new OrdersNotPreparedAdapter(orderList);
        this.orderService = new OrderService(
                orderList,
                this,
                getString(R.string.date_format_US));
        setRecyclerView(recyclerView, adapter);

        this.orderService.retrieveOrdersNotPrepared();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onGettingOrders() {
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onMarkingOrderAsDone(long orderId) {
        this.orderService.retrieveOrdersNotPrepared();
    }
}
