package com.companysf.cardview_slider.Activity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.ui.TextViewWithDefaultColor;

public interface SelectedBtn {
    default void changeButtonStyleToSelected(Context context, TextView button) {
        button.setBackground(
                ContextCompat.getDrawable(context, R.drawable.selected_btn)
        );
        button.setTextColor(
                ContextCompat.getColor(context, R.color.colorAccent)
        );
    }

    default void changeButtonsStylesToNotSelected(Context context, TextViewWithDefaultColor...textViewWithDefaultColor) {
        for (TextViewWithDefaultColor textViewWithDefaultColorItem : textViewWithDefaultColor) {
            textViewWithDefaultColorItem.getTextView().setBackground(
                    ContextCompat.getDrawable(context, R.drawable.not_selected_btn)
            );
            textViewWithDefaultColorItem.getTextView().setTextColor(textViewWithDefaultColorItem.getDefaultColor());
        }
    }
}
