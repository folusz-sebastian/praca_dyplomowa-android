package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.LastTransactionsAdapter;
import com.companysf.cardview_slider.connection.LastTransactionsConnection;
import com.companysf.cardview_slider.entity.model.order.DishOrder2;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.Toolbar;
import java.util.ArrayList;
import java.util.List;

public class LastTransactionsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last_transactions_list);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        RecyclerView recyclerView = findViewById(R.id.transactionsListRecyclerView);
        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

        toolbarWrap.setToolbar(this);

        List<DishOrder2> lastTransactionList = new ArrayList<>();
        LastTransactionsAdapter adapter = new LastTransactionsAdapter(lastTransactionList);
        LastTransactionsConnection lastTransactionsConnection = new LastTransactionsConnection(
                getString(R.string.date_format_with_hour),
                lastTransactionList,
                adapter
        );

        setRecyclerView(recyclerView, adapter);

        lastTransactionsConnection.getLastTransactions();
    }

    private void setRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
