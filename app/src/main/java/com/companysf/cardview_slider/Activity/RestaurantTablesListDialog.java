package com.companysf.cardview_slider.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.connection.NewRestaurantTableConnection;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.entity.model.restaurantTable.TablePlace;
import com.companysf.cardview_slider.entity.ui.TextViewWithDefaultColor;
import com.companysf.cardview_slider.services.NewRestaurantTableService;

public class RestaurantTablesListDialog extends DialogFragment implements SelectedBtn {
    private Context context;
    private int selectedBtnIndex;
    private TextView numberOfChairsTextView;
    private NewRestaurantTableService service;
    private RestaurantTable newRestaurantTable;
    private NewRestaurantTableConnection.RestaurantTableListener restaurantTableListener;

    public RestaurantTablesListDialog() {
    }

    public static RestaurantTablesListDialog newInstance() {
        RestaurantTablesListDialog frag = new RestaurantTablesListDialog();
        frag.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NewRestaurantTableConnection.RestaurantTableListener) {
            restaurantTableListener = (NewRestaurantTableConnection.RestaurantTableListener) context;
        } else {
            throw new RuntimeException(
                     context.getString(R.string.exception_on_not_implementing_listener3, context.toString())
            );
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newRestaurantTable = new RestaurantTable(0, 1, new TablePlace(0, "", true));
        service = new NewRestaurantTableService(this.newRestaurantTable, this.restaurantTableListener);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.dialog_restaurant_table, container);
        this.context = view.getContext();
        getDialog().setTitle(context.getString(R.string.restaurant_tables_list_dialog_title));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        selectedBtnIndex = -1;

        numberOfChairsTextView = view.findViewById(R.id.chairsCount);
        ImageView removeChairBtn = view.findViewById(R.id.removeChair);
        ImageView addChairBtn = view.findViewById(R.id.addChair);
        Button approveBtn = view.findViewById(R.id.approveButton);
        Button cancelBtn = view.findViewById(R.id.cancelButton);
        TextViewWithDefaultColor tableNextToWindowOptionBtn = new TextViewWithDefaultColor(view.findViewById(R.id.nextToWindowPlace));
        TextViewWithDefaultColor tableNextToWallOptionBtn = new TextViewWithDefaultColor(view.findViewById(R.id.nextToWallPlace));
        TextViewWithDefaultColor tableOnCenterOfRoomOptionBtn = new TextViewWithDefaultColor(view.findViewById(R.id.inCenterOfRoomPlace));

        addChairBtn.setOnClickListener(v -> {
            service.addChair();
            numberOfChairsTextView.setText(
                    context.getString(R.string.number_of_chairs, this.newRestaurantTable.getChairs())
            );
        });

        removeChairBtn.setOnClickListener(v -> {
            service.removeChair();
            numberOfChairsTextView.setText(
                    context.getString(R.string.number_of_chairs, this.newRestaurantTable.getChairs())
            );

        });

        highlightClickedBtnAndDisableOthers(
                tableNextToWindowOptionBtn, tableNextToWallOptionBtn, tableOnCenterOfRoomOptionBtn
        );

        cancelBtn.setOnClickListener(v -> getDialog().dismiss());

        approveBtn.setOnClickListener(v -> {
            if (selectedBtnIndex == -1) {
                Toast.makeText(context, context.getString(R.string.error_on_not_choosing_table_place), Toast.LENGTH_SHORT).show();
            } else {
                service.save();
                getDialog().dismiss();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        restaurantTableListener = null;
    }

    private void highlightClickedBtnAndDisableOthers(TextViewWithDefaultColor...tableLocationOptionBtn) {
        for (int i = 0; i < tableLocationOptionBtn.length; i++) {
            int finalI = i;
            tableLocationOptionBtn[i].getTextView().setOnClickListener(v -> {
                changeButtonsStylesToNotSelected(context, tableLocationOptionBtn);
                changeButtonStyleToSelected(context, tableLocationOptionBtn[finalI].getTextView());
                selectedBtnIndex = finalI;
                service.settingTablePlaceName(tableLocationOptionBtn[finalI].getTextView().getText().toString());
            });
        }
    }
}
