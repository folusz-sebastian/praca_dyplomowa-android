package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.companysf.cardview_slider.Activity.mainActivity.CustomerMainActivity;
import com.companysf.cardview_slider.Activity.mainActivity.EmployeeMainActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.companysf.cardview_slider.services.UserService;

public class ChooseRoleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_role);

        UserRealm user = UserService.getUserFromRealm();
        if (user == null) {
            switchToLoginActivity();
        } else {
            TextView welcomeText = findViewById(R.id.welcomeText);
            TextView logoutBtn = findViewById(R.id.logoutBtn);
            Button customerRoleBtn = findViewById(R.id.customerRoleBtn);
            Button employeeRoleBtn = findViewById(R.id.employeeRoleBtn);

            welcomeText.setText(
                    getString(R.string.welcome_text, user.getName())
            );

            logoutBtn.setOnClickListener(v -> {
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
            });

            customerRoleBtn.setOnClickListener(view -> switchToCustomerMainActivity());
            employeeRoleBtn.setOnClickListener(view -> switchToEmployeeMainActivity());
        }
    }

    private void switchToCustomerMainActivity() {
        Intent intent = new Intent(this, CustomerMainActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToEmployeeMainActivity() {
        Intent intent = new Intent(this, EmployeeMainActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
