package com.companysf.cardview_slider.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.connection.MeasurementUnitConnection;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;
import com.companysf.cardview_slider.entity.model.dish.Ingredient;
import com.companysf.cardview_slider.entity.model.dish.MeasurementUnit;
import com.companysf.cardview_slider.views.NumberEditText;

import java.util.ArrayList;
import java.util.List;

import static solid.collectors.ToSolidList.toSolidList;
import static solid.stream.Stream.stream;

public class CreateDishAddIngredientDialog extends DialogFragment implements MeasurementUnitConnection.MeasurementUnitListener {

    private OnFragmentInteractionListener mListener;
    private Button cancelButton;
    private Button approveButton;
    private EditText ingredientNameInput;
    private NumberEditText ingredientQuantityInput;
    private Spinner measurementUnitSpinner;
    private List<MeasurementUnit> measurementUnitList;
    private Context context;

    public CreateDishAddIngredientDialog() {
        // Required empty public constructor
    }

    public static CreateDishAddIngredientDialog newInstance() {
        CreateDishAddIngredientDialog frag = new CreateDishAddIngredientDialog();
        frag.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(
                    getString(R.string.exception_on_not_implementing_listener, context.toString())
            );
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_create_dish_add_ingredient, container, false);
        this.context = view.getContext();
        getDialog().setTitle(context.getString(R.string.create_dish_add_ingredient_dialog_title));
        this.cancelButton = view.findViewById(R.id.cancelButton);
        this.approveButton = view.findViewById(R.id.approveButton);
        this.ingredientNameInput = view.findViewById(R.id.ingredientNameInput);
        this.ingredientQuantityInput = view.findViewById(R.id.ingredientQuantityInput);
        this.measurementUnitSpinner = view.findViewById(R.id.measurementUnitSpinner);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        measurementUnitList = new ArrayList<>();

        MeasurementUnitConnection measurementUnitConnection = new MeasurementUnitConnection(this, measurementUnitList);
        measurementUnitConnection.getMeasurementUnits();

        cancelButton.setOnClickListener(view -> dismiss());
        approveButton.setOnClickListener(view -> saveIngredient());
    }

    private void saveIngredient() {
        if (ingredientNameInput.getText().toString().isEmpty() ||
                ingredientQuantityInput.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.error_on_empty_edit_texts), Toast.LENGTH_SHORT).show();
        } else {
            if (mListener != null) {
                DishIngredient newDishIngredient = createDishIngredient();

                mListener.addNewIngredientToList(newDishIngredient);
                dismiss();
            }
        }
    }

    private DishIngredient createDishIngredient() {
        MeasurementUnit measurementUnit = measurementUnitList.get(
                measurementUnitSpinner.getSelectedItemPosition()
        );

        double ingredientQuantity = ingredientQuantityInput.convertToDouble();

        return new DishIngredient(
                new Ingredient(
                        ingredientNameInput.getText().toString(),
                        measurementUnit
                ),
                ingredientQuantity
        );
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuccessGettingMeasurementUnits() {
        initMeasurementUnitSpinner();
    }

    @Override
    public void onFailureGettingMeasurementUnits() {

    }

    public interface OnFragmentInteractionListener {
        void addNewIngredientToList(DishIngredient dishIngredient);
    }

    private void initMeasurementUnitSpinner() {
        List<String> measurementUnitListStrings = stream(measurementUnitList).map(MeasurementUnit::getName).collect(toSolidList());
        SpinnerAdapter spinnerAdapter = new ArrayAdapter<>(this.context, android.R.layout.simple_list_item_1, measurementUnitListStrings);
        measurementUnitSpinner.setAdapter(spinnerAdapter);
    }
}
