package com.companysf.cardview_slider.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.RestaurantTablesInReservationAdapter;
import com.companysf.cardview_slider.app.Restaurant;
import com.companysf.cardview_slider.connection.ReservationConnection;
import com.companysf.cardview_slider.connection.RestaurantTablesConnection;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.entity.model.reservation.realm.ReservationRealm;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.entity.ui.TextViewWithDefaultColor;
import com.companysf.cardview_slider.services.ReservationService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.utils.DateUtils;
import com.companysf.cardview_slider.views.Toolbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ReservationActivity extends AppCompatActivity implements SelectedBtn, ReservationConnection.ReservationListener {

    private DatePickerDialog datepicker;
    private Spinner hoursListSpinner;
    private Spinner reservationDurationListSpinner;
    private RestaurantTablesInReservationAdapter restaurantTablesAdapter;
    private Reservation reservation;
    private ReservationService reservationService;
    private ArrayAdapter<Integer> durationListSpinnerAdapter;
    private ArrayList<Integer> durations;
    private final Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat dateFormatYear;
    private SimpleDateFormat dateFormatMonth;
    private SimpleDateFormat dateFormatDay;
    private List<Integer> restaurantWorkingHours;
    private List<String> restaurantWorkingHoursWithSuffix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        TextViewWithDefaultColor tomorrowBtn = new TextViewWithDefaultColor(findViewById(R.id.tomorrowBtn));
        TextViewWithDefaultColor dayAfterTomorrowBtn = new TextViewWithDefaultColor(findViewById(R.id.dayAfterTomorrowBtn));
        TextViewWithDefaultColor chooseDateBtn = new TextViewWithDefaultColor(findViewById(R.id.chooseDateBtn));
        List<TextViewWithDefaultColor> allCalendarDayButtons = new ArrayList<>();
        allCalendarDayButtons.add(tomorrowBtn);
        allCalendarDayButtons.add(dayAfterTomorrowBtn);
        allCalendarDayButtons.add(chooseDateBtn);

        changeButtonStyleToSelected(this, tomorrowBtn.getTextView());

        this.hoursListSpinner = findViewById(R.id.hoursList);
        this.reservationDurationListSpinner = findViewById(R.id.reservationDurationList);
        RecyclerView recyclerView = findViewById(R.id.restaurantTablesListRecyclerView);
        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

        toolbarWrap.setToolbar(this);
        toolbarWrap.enableToolbarUpNavigation(this);

        this.restaurantWorkingHours = Restaurant.hoursList();
        this.restaurantWorkingHoursWithSuffix = Restaurant.hoursList(getString(R.string.hours_suffix));
        this.reservation = new Reservation();
        this.durations = new ArrayList<>();
        List<RestaurantTable> restaurantTableList = new ArrayList<>();

        setHoursListAdapter();
        setStayDurationsListAdapter();

        this.restaurantTablesAdapter = new RestaurantTablesInReservationAdapter(restaurantTableList, this.reservation, this);

        this.reservationService = new ReservationService(
                this.reservation,
                this.durationListSpinnerAdapter,
                this.durations,
                new RestaurantTablesConnection(
                        restaurantTableList,
                        this.restaurantTablesAdapter),
                restaurantTableList,
                restaurantTablesAdapter
        );
        this.reservationService.updateDurationList();

        setRecyclerView(recyclerView);

        setDateFormats();

        this.reservationService.retrieveRestaurantTables(dateFormatYear, dateFormatMonth, dateFormatDay);

        highlightOnClickAndDisableOthers(allCalendarDayButtons, tomorrowBtn, dayAfterTomorrowBtn);
        chooseDateBtn.getTextView().setOnClickListener(v -> {
            this.calendar.setTime(DateUtils.TOMORROW);

            int tomorrowYear = calendar.get(Calendar.YEAR);
            int tomorrowMonth = calendar.get(Calendar.MONTH);
            int tomorrowDay = calendar.get(Calendar.DAY_OF_MONTH);

            long today = Calendar.getInstance().getTimeInMillis();
            final long oneDay = 24 * 60 * 60 * 1000L;

            datepicker = new DatePickerDialog(
                    ReservationActivity.this,
                    (view, choosedYear, choosedMonthOfYear, choosedDayOfMonth) -> {
                        Calendar choosedDate = DateUtils.setDate(choosedYear, choosedMonthOfYear, choosedDayOfMonth);

                        changeButtonsStylesToNotSelected(this, tomorrowBtn, dayAfterTomorrowBtn, chooseDateBtn);
                        if (DateUtils.isTomorrow(choosedDate)) {
                            changeButtonStyleToSelected(this, tomorrowBtn.getTextView());
                            chooseDateBtn.getTextView().setText("");
                        } else if (DateUtils.isDayAfterTomorrow(choosedDate)) {
                            changeButtonStyleToSelected(this, dayAfterTomorrowBtn.getTextView());
                            chooseDateBtn.getTextView().setText("");
                        } else {
                            changeButtonStyleToSelected(this, chooseDateBtn.getTextView());

                            SimpleDateFormat datePLFormat = new SimpleDateFormat(
                                    getString(R.string.date_format_PL),
                                    new Locale(getString(R.string.polish), getString(R.string.poland)));

                            chooseDateBtn.getTextView().setText(
                                    datePLFormat.format(choosedDate.getTime())
                            );

                        }
                        reservation.setDate(choosedDate.getTime());
                        reservationService.retrieveRestaurantTables(dateFormatYear, dateFormatMonth, dateFormatDay);
                    },
                    tomorrowYear,
                    tomorrowMonth,
                    tomorrowDay
            );

            datepicker.getDatePicker().setMinDate(today + oneDay);
            datepicker.getDatePicker().setMaxDate(today + 7 * oneDay);
            datepicker.show();
        });

        this.hoursListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int beginningHour = restaurantWorkingHours.get(position);
                reservation.setBeginningHour(beginningHour);
                reservationService.updateDurationList();
                reservationService.retrieveRestaurantTables(dateFormatYear, dateFormatMonth, dateFormatDay);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        this.reservationDurationListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reservation.setDuration(durations.get(position));
                reservationService.retrieveRestaurantTables(dateFormatYear, dateFormatMonth, dateFormatDay);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setDateFormats() {
        this.dateFormatYear = new SimpleDateFormat(
                getString(R.string.date_format_year),
                new Locale(getString(R.string.polish), getString(R.string.poland))
        );
        this.dateFormatMonth = new SimpleDateFormat(
                getString(R.string.date_format_month),
                new Locale(getString(R.string.polish), getString(R.string.poland))
        );
        this.dateFormatDay = new SimpleDateFormat(
                getString(R.string.date_format_day),
                new Locale(getString(R.string.polish), getString(R.string.poland))
        );
    }

    private void setHoursListAdapter() {
        ArrayAdapter<String> hoursListSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, restaurantWorkingHoursWithSuffix);
        hoursListSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.hoursListSpinner.setAdapter(hoursListSpinnerAdapter);
    }

    private void setStayDurationsListAdapter() {
        this.durationListSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, this.durations);
        this.durationListSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.reservationDurationListSpinner.setAdapter(durationListSpinnerAdapter);
    }

    private void setRecyclerView(RecyclerView tablesListRecyclerView) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        tablesListRecyclerView.setLayoutManager(layoutManager);
        tablesListRecyclerView.setNestedScrollingEnabled(false);
        tablesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tablesListRecyclerView.setAdapter(this.restaurantTablesAdapter);
    }

    private void highlightOnClickAndDisableOthers(List<TextViewWithDefaultColor> buttonsToDisable, TextViewWithDefaultColor... activeButtons) {
        TextViewWithDefaultColor[] buttonsToDisableArray = new TextViewWithDefaultColor[buttonsToDisable.size()];
        for (int i = 0; i < activeButtons.length; i++) {
            int finalI = i;
            activeButtons[i].getTextView().setOnClickListener(v -> {
                changeButtonsStylesToNotSelected(this, buttonsToDisable.toArray(buttonsToDisableArray));
                changeButtonStyleToSelected(this, activeButtons[finalI].getTextView());
                if (finalI == 0) {
                    this.reservation.setDate(DateUtils.TOMORROW);
                } else {
                    this.reservation.setDate(DateUtils.DAY_AFTER_TOMORROW);
                }
                this.reservationService.retrieveRestaurantTables(dateFormatYear, dateFormatMonth, dateFormatDay);
            });
        }
    }

    @Override
    public void onGettingReservation(ReservationRealm body) {
        Toast.makeText(this, getString(R.string.success_on_getting_reservation), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MyReservationsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
