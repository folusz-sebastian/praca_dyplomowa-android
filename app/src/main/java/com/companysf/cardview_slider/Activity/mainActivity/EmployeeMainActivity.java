package com.companysf.cardview_slider.Activity.mainActivity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.companysf.cardview_slider.Activity.CreateDishActivity;
import com.companysf.cardview_slider.Activity.LoginActivity;
import com.companysf.cardview_slider.Activity.OrdersNotPreparedActivity;
import com.companysf.cardview_slider.Activity.RestaurantMenuActivity;
import com.companysf.cardview_slider.Activity.RestaurantTablesListActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.DishCardMainSiteAdapter;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.DishConnection;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.utils.PageTransformer;
import com.companysf.cardview_slider.utils.SessionManager;
import com.companysf.cardview_slider.views.Toolbar;

public class EmployeeMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_employee);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        } else {
            UserService.saveLoginSessionUserRole(AppConfig.USER_ROLE_EMPLOYEE);

            ViewPager mViewPager = findViewById(R.id.viewPager);
            Button tablesManagementBtn = findViewById(R.id.tablesManagementBtn);
            Button ordersToBeCarriedOutBtn = findViewById(R.id.ordersToBeCarriedOutBtn);
            Button addDishBtn = findViewById(R.id.addDishBtn);
            TextView menuBtn = findViewById(R.id.menuBtn);
            ImageView pizzaBtn = findViewById(R.id.pizzaCategory);
            ImageView dinnerBtn = findViewById(R.id.dinnerCategory);
            ImageView dessertBtn = findViewById(R.id.dessertCategory);
            ImageView fishBtn = findViewById(R.id.fishCategory);
            ImageView soupBtn = findViewById(R.id.soupCategory);

            Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);
            toolbarWrap.setToolbar(this);

            DishCardMainSiteAdapter dishCardMainSiteAdapter = new DishCardMainSiteAdapter();
            mViewPager.setAdapter(dishCardMainSiteAdapter);
            DishConnection dishConnection = new DishConnection(dishCardMainSiteAdapter);

            PageTransformer pageTransformer = new PageTransformer(mViewPager, dishCardMainSiteAdapter);
            mViewPager.setPageTransformer(false, pageTransformer);
            mViewPager.setOffscreenPageLimit(3);

            dishConnection.getDishes(false, null);

            tablesManagementBtn.setOnClickListener(view -> switchToTablesManagementList());
            ordersToBeCarriedOutBtn.setOnClickListener(view -> switchToOrdersNotPreparedActivity());
            addDishBtn.setOnClickListener(view -> switchToCreateDishActivity());
            menuBtn.setOnClickListener(view -> switchToMenu());
            pizzaBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_PIZZA));
            dinnerBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_DINNER));
            dessertBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_DESSERT));
            fishBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_FISH));
            soupBtn.setOnClickListener(view -> switchToMenu(AppConfig.DISH_CATEGORY_SOUP));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuLogOutIcon) {
            switchToLoginActivity();
            UserService.removeUserFromDevice();
            new UserService().logoutUserFromBackend();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchToTablesManagementList() {
        Intent intent = new Intent(this, RestaurantTablesListActivity.class);
        startActivity(intent);
    }

    private void switchToOrdersNotPreparedActivity() {
        Intent intent = new Intent(this, OrdersNotPreparedActivity.class);
        startActivity(intent);
    }


    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToCreateDishActivity() {
        Intent intent = new Intent(this, CreateDishActivity.class);
        startActivity(intent);
    }

    private void switchToMenu() {
        switchToMenu(null);
    }

    private void switchToMenu(String category) {
        Intent intent = new Intent(this, RestaurantMenuActivity.class);
        intent.putExtra(getString(R.string.intent_extra_category), category);
        startActivity(intent);
    }
}
