package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.MyReservationsAdapter;
import com.companysf.cardview_slider.connection.ReservationConnection;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.services.ReservationService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class MyReservationsActivity extends AppCompatActivity implements ReservationConnection.ReservationsListener {

    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reservations);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        RecyclerView recyclerView = findViewById(R.id.myReservationsRecyclerView);
        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

        toolbarWrap.setToolbar(this);
        toolbarWrap.enableToolbarUpNavigation(this);

        List<Reservation> reservationList = new ArrayList<>();
        this.adapter = new MyReservationsAdapter(reservationList);
        ReservationService reservationService = new ReservationService(
                reservationList,
                this,
                getString(R.string.date_format_US)
        );
        setRecyclerView(recyclerView, adapter);
        reservationService.retrieveMyReservations();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onGettingReservations() {
        this.adapter.notifyDataSetChanged();
    }
}
