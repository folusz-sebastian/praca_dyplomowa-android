package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.NetworkResponse;
import com.companysf.cardview_slider.Activity.mainActivity.CustomerMainActivity;
import com.companysf.cardview_slider.Activity.mainActivity.EmployeeMainActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.connection.UserConnection;
import com.companysf.cardview_slider.entity.model.user.LoginForm;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.utils.Utils;

public class LoginActivity extends AppCompatActivity implements UserConnection.LoginListener {
    private UserService userService;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (UserService.getUserFromRealm() != null) {
            switchLoggedInUserToSuitableActivity();
        }

        TextView registerBtn = findViewById(R.id.registerActivityBtn);
        this.loginBtn = findViewById(R.id.loginBtn);
        EditText inputEmail = findViewById(R.id.inputEmail);
        EditText inputPassword = findViewById(R.id.inputPassword);

        this.userService = new UserService(this, getString(R.string.date_format_US));

        registerBtn.setOnClickListener(v -> switchToRegister());
        this.loginBtn.setOnClickListener(v -> onLogin(inputEmail, inputPassword));
    }

    private void onLogin(EditText email, EditText password) {
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();

        if (!Utils.isEmail(emailText)) {
            Toast.makeText(this, getString(R.string.error_on_empty_email), Toast.LENGTH_SHORT).show();
        } else if (passwordText.isEmpty()) {
            Toast.makeText(this, getString(R.string.error_on_empty_password), Toast.LENGTH_SHORT).show();
        } else {
            this.loginBtn.setEnabled(false);
            Log.d("onLoginMethod", "onloginmethod - jestem w else");
            LoginForm loginForm = new LoginForm(emailText, passwordText);
            this.userService.retrieveUser(loginForm);
        }
    }

    private void switchToRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void switchToChooseRoleActivity() {
        Intent intent = new Intent(this, ChooseRoleActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToCustomerMain() {
        Intent intent = new Intent(this, CustomerMainActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToEmployeeMain() {
        Intent intent = new Intent(this, EmployeeMainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResponse(UserRealm userRealm, String authenticationKey) {
        this.loginBtn.setEnabled(true);
        UserService.saveInDevice(userRealm);
        UserService.saveInDevice(authenticationKey);

        switchLoggedInUserToSuitableActivity();
    }

    private void switchLoggedInUserToSuitableActivity() {
        if (UserService.userHasRole(AppConfig.USER_ROLE_EMPLOYEE)) {
            if (UserService.userHasRole(AppConfig.USER_ROLE_CUSTOMER)) {
                switchToChooseRoleActivity();
            } else {
                switchToEmployeeMain();
            }
        } else if (UserService.userHasRole(AppConfig.USER_ROLE_CUSTOMER)) {
            switchToCustomerMain();
        }
    }

    @Override
    public void onError(NetworkResponse networkResponse) {
        this.loginBtn.setEnabled(true);
        if (networkResponse != null && networkResponse.statusCode == 401) {
            Toast.makeText(this, "Niepoprawne dane", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Błąd serwera", Toast.LENGTH_SHORT).show();
        }
    }
}
