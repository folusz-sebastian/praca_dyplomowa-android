package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.DishIngredientCardAdapter;
import com.companysf.cardview_slider.connection.DishCategoryConnection;
import com.companysf.cardview_slider.connection.DishConnection;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.companysf.cardview_slider.entity.model.dish.DishCategory;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;
import com.companysf.cardview_slider.entity.others.LocalStorage;
import com.companysf.cardview_slider.services.DishCategoryService;
import com.companysf.cardview_slider.services.ImageUploadService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.NumberEditText;
import com.companysf.cardview_slider.views.Toolbar;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import io.realm.RealmList;
import static solid.collectors.ToSolidList.toSolidList;
import static solid.stream.Stream.stream;

public class CreateDishActivity extends AppCompatActivity
        implements CreateDishAddIngredientDialog.OnFragmentInteractionListener,
        ImageUploadService.ImageUploadListener, DishCategoryConnection.DishCategoryListener,
        DishConnection.DishSavingListener {
    private Spinner dishCategorySpinner;
    private static final int PICK_IMAGE_REQUEST = 1;
    private TextView chooseImageBtn;
    private Button addNewDishBtn;
    private RealmList<DishIngredient> dishIngredientList;
    private LocalStorage localStorage;
    private ImageView dishImage;
    private RecyclerView.Adapter adapter;
    private EditText dishNameInput, dishDescriptionInput;
    private NumberEditText dishPriceInput;
    private ImageUploadService imageUploadService;
    private List<DishCategory> dishCategoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_dish);

        if (UserService.getUserFromRealm() == null) {
            switchToLoginActivity();
        }

        Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);
        toolbarWrap.setToolbar(this);
        toolbarWrap.enableToolbarUpNavigation(this);

        dishNameInput = findViewById(R.id.dishNameInput);
        dishPriceInput = findViewById(R.id.dishPriceInput);
        dishCategorySpinner = findViewById(R.id.dishCategorySpinner);
        chooseImageBtn = findViewById(R.id.chooseImageBtn);
        dishImage = findViewById(R.id.dishImage);
        addNewDishBtn = findViewById(R.id.addNewDish);
        dishDescriptionInput = findViewById(R.id.dishDescriptionInput);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ImageView addIngredientBtn = findViewById(R.id.addIngredientBtn);
        initDishCategorySpinner();

        this.dishIngredientList = new RealmList<>();
        this.adapter = new DishIngredientCardAdapter(dishIngredientList, true);

        this.imageUploadService = new ImageUploadService(
                getContentResolver(),
                this,
                getString(R.string.firebase_location)
        );

        setRecyclerView(recyclerView, adapter);
        adapter.notifyDataSetChanged();

        this.chooseImageBtn.setOnClickListener(view -> openFileChooser());
        addIngredientBtn.setOnClickListener(view -> showDialog());
        this.addNewDishBtn.setOnClickListener(view -> saveDishInDB());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveDishInDB() {
        if (dishNameInput.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_empty_dish_name), Toast.LENGTH_SHORT).show();
        } else if (dishPriceInput.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_empty_dish_price), Toast.LENGTH_SHORT).show();
        } else if (dishDescriptionInput.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_empty_dish_description), Toast.LENGTH_SHORT).show();
        } else if (localStorage.getUri() == null) {
            Toast.makeText(this, getString(R.string.error_empty_dish_image), Toast.LENGTH_SHORT).show();
        } else if (dishIngredientList.isEmpty()) {
            Toast.makeText(this, getString(R.string.error_empty_ingredient_list), Toast.LENGTH_SHORT).show();
        } else {
            this.addNewDishBtn.setEnabled(false);
            this.imageUploadService.uploadFile(this.localStorage.getUri());
        }
    }

    private Dish createDish(String imageUrl) {
        Dish dish = new Dish();
        dish.setName(dishNameInput.getText().toString());
        dish.setPrice(
                dishPriceInput.convertToDouble()
        );
        dish.setImageUrl(imageUrl);
        dish.setCategory(
                dishCategoryList.get(dishCategorySpinner.getSelectedItemPosition())
        );
        dish.setDescription(
                dishDescriptionInput.getText().toString()
        );
        dish.setDishIngredients(dishIngredientList);
        return dish;
    }

    private void showDialog() {
        FragmentManager fm = getSupportFragmentManager();
        CreateDishAddIngredientDialog dialog = CreateDishAddIngredientDialog.newInstance();
        dialog.show(fm, getString(R.string.dialog_tag_create_dish_add_ingredient));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (
                requestCode == PICK_IMAGE_REQUEST &&
                        resultCode == RESULT_OK &&
                        data != null &&
                        data.getData() != null
        ) {
            localStorage = new LocalStorage(data.getData());
            Picasso.get()
                    .load(localStorage.getUri())
                    .into(dishImage);
            dishImage.setVisibility(View.VISIBLE);
            chooseImageBtn.setText(getString(R.string.change_image_btn_text));
        }
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType(getString(R.string.intent_type_image));
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void initDishCategorySpinner() {
        this.dishCategoryList = new ArrayList<>();
        DishCategoryService dishCategoryService = new DishCategoryService(dishCategoryList, this);
        dishCategoryService.retrieveDishCategories();
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void switchToRestaurantMenuActivity() {
        Intent intent = new Intent(this, RestaurantMenuActivity.class);
        startActivity(intent);
        finish();
    }

    private void setRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addNewIngredientToList(DishIngredient dishIngredient) {
        dishIngredientList.add(dishIngredient);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onUploadImageSuccess(String imageUrl) {
        Dish dish = createDish(imageUrl);
        DishConnection dishConnection = new DishConnection(this);
        dishConnection.saveDish(dish);
    }

    @Override
    public void onUploadImageFailure() {
        Toast.makeText(this, getString(R.string.error_on_upload_image), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGettingDishCategories() {
        List<String> dishCategoryListStrings = stream(dishCategoryList).map(DishCategory::getName).collect(toSolidList());

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, dishCategoryListStrings);
        dishCategorySpinner.setAdapter(spinnerAdapter);
    }

    @Override
    public void onFailureGettingDishCategories() {

    }

    @Override
    public void onSuccessSavingDish() {
        this.addNewDishBtn.setEnabled(true);
        Toast.makeText(this, getString(R.string.success_on_saving_dish), Toast.LENGTH_SHORT).show();
        switchToRestaurantMenuActivity();
    }

    @Override
    public void onErrorSavingDish() {
        this.addNewDishBtn.setEnabled(true);
        Toast.makeText(this, getString(R.string.error_on_saving_dish), Toast.LENGTH_SHORT).show();
    }
}
