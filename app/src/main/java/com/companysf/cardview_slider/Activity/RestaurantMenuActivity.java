package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.adapter.DishCardAdapter;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.companysf.cardview_slider.entity.model.order.Realm.RealmDishOrder;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.companysf.cardview_slider.services.DishOrderService;
import com.companysf.cardview_slider.services.DishService;
import com.companysf.cardview_slider.services.UserService;
import com.companysf.cardview_slider.views.OrdersInfoBar;
import com.companysf.cardview_slider.views.Toolbar;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

public class RestaurantMenuActivity extends AppCompatActivity {
    private OrdersInfoBar ordersInfoBar;
    private String category;
    private RealmResults<RealmDishOrder> dishOrderRealmResults;
    private OrderedRealmCollectionChangeListener<RealmResults<RealmDishOrder>> orderItemsChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_menu);

        UserRealm user = UserService.getUserFromRealm();
        if (user == null) {
            switchToLoginActivity();
        } else {
            ordersInfoBar = findViewById(R.id.orderInfoBar);
            Toolbar toolbarWrap = findViewById(R.id.toolbarWrap);

            toolbarWrap.setToolbar(this);
            toolbarWrap.enableToolbarUpNavigation(this);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(
                        getString(R.string.restaurant_menu_activity_title)
                );
            }
            getDishCategoryIntentExtra();
            ordersInfoBar.setOnClickListener(v -> showCartItemsFragment());

            RecyclerView dishes_list = findViewById(R.id.dishesList);
            List<Dish> dishList = new ArrayList<>();
            DishCardAdapter dishCardAdapter = new DishCardAdapter(dishList);
            DishService dishService = new DishService(
                    dishList,
                    dishCardAdapter
            );

            setRecyclerView(dishes_list, dishCardAdapter);

            dishService.retrieveDishes(this.category);

            dishOrderRealmResults = DishOrderService.findAllAsync();
            orderItemsChangeListener = (orderItems, changeSet) -> ordersInfoBar.setData(
                    DishOrderService.countOrderItems(dishOrderRealmResults),
                    DishOrderService.countTotalPrice(dishOrderRealmResults)
            );
            dishOrderRealmResults.addChangeListener(orderItemsChangeListener);

            if (UserService.userLoggedInAs(AppConfig.USER_ROLE_EMPLOYEE)) {
                ordersInfoBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        RealmResults<RealmDishOrder> dishOrderRealmResults = DishOrderService.findAll();
        ordersInfoBar.setData(
                DishOrderService.countOrderItems(dishOrderRealmResults),
                DishOrderService.countTotalPrice(dishOrderRealmResults)
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (dishOrderRealmResults != null) {
            dishOrderRealmResults.removeChangeListener(orderItemsChangeListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menuLogOutIcon:
                switchToLoginActivity();
                UserService.removeUserFromDevice();
                new UserService().logoutUserFromBackend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setRecyclerView(RecyclerView dishes_list, DishCardAdapter dishCardAdapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        dishes_list.setLayoutManager(layoutManager);
        dishes_list.setNestedScrollingEnabled(false);
        dishes_list.setItemAnimator(new DefaultItemAnimator());
        dishes_list.setAdapter(dishCardAdapter);
    }

    private void showCartItemsFragment() {
        OrderFragment fragment = new OrderFragment();
        fragment.show(getSupportFragmentManager(), fragment.getTag());
    }

    private void getDishCategoryIntentExtra() {
        this.category = null;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.category = bundle.getString(getString(R.string.intent_extra_category));
            if (getSupportActionBar() != null && this.category != null) {
                getSupportActionBar().setTitle(this.category);
            }
        }
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
