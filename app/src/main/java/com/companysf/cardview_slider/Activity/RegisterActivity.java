package com.companysf.cardview_slider.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.companysf.cardview_slider.Activity.mainActivity.CustomerMainActivity;
import com.companysf.cardview_slider.R;
import com.companysf.cardview_slider.connection.RegisterConnection;
import com.companysf.cardview_slider.entity.model.user.RegisteredUser;
import com.companysf.cardview_slider.services.RegisterService;
import com.companysf.cardview_slider.utils.Utils;

public class RegisterActivity extends AppCompatActivity implements RegisterConnection.RegisterListener {
    private EditText inputName;
    private EditText inputSurname;
    private EditText inputEmail;
    private EditText inputPassword;
    private RegisterService registerService;
    private RegisteredUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        this.inputName = findViewById(R.id.inputName);
        this.inputSurname = findViewById(R.id.inputSurname);
        this.inputEmail = findViewById(R.id.inputEmail);
        this.inputPassword = findViewById(R.id.inputPassword);
        Button registerBtn = findViewById(R.id.registerBtn);
        TextView loginActivityBtn = findViewById(R.id.loginActivityBtn);

        this.user = new RegisteredUser();
        this.registerService = new RegisterService(this.user, this);

        loginActivityBtn.setOnClickListener(v -> switchToLoginActivity());
        registerBtn.setOnClickListener(v -> onRegister());
    }

    private void onRegister() {
        this.user.setName(inputName.getText().toString());
        this.user.setLastName(inputSurname.getText().toString());
        this.user.setEmail(inputEmail.getText().toString());
        this.user.setPassword(inputPassword.getText().toString());

        if (!Utils.isEmail(user.getEmail())) {
            Toast.makeText(this, getString(R.string.error_on_empty_email), Toast.LENGTH_SHORT).show();
        } else if (user.getName().isEmpty() ||
                user.getLastName().isEmpty() ||
                user.getPassword().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_on_empty_edit_texts), Toast.LENGTH_SHORT).show();
        } else {
            registerService.signUpUser();
        }
    }

    private void switchToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void switchToMainActivity() {
        Intent intent = new Intent(this, CustomerMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResponse() {
        Toast.makeText(
                this,
                "konto zostało pomyślnie utworzone",
                Toast.LENGTH_LONG
        ).show();
        switchToMainActivity();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG
        ).show();
    }
}
