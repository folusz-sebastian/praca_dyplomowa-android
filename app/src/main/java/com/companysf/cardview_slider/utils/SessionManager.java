package com.companysf.cardview_slider.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.companysf.cardview_slider.app.AppController;

public class SessionManager {
    private static final SessionManager ourInstance =
            new SessionManager(AppController.getInstance().getApplicationContext());
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static final String PREF_NAME = "app_prefs";
    private static final int PRIVATE_MODE = 0;
    private static final String AUTHORIZATION_KEY = "authorization";

    public static SessionManager getInstance() {
        return ourInstance;
    }

    private SessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public void saveAuthKey(String authKey) {
        editor.putString(AUTHORIZATION_KEY, authKey);
        editor.commit();
    }

    public String getAuthKey() {
        return sharedPreferences.getString(AUTHORIZATION_KEY, null);
    }

    public void clearData() {
        editor.clear().commit();
    }
}
