package com.companysf.cardview_slider.utils;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.View;

import com.companysf.cardview_slider.adapter.DishCardMainSiteAdapter;

public class PageTransformer implements ViewPager.OnPageChangeListener, ViewPager.PageTransformer {
    private DishCardMainSiteAdapter dishCardMainSiteAdapter;
    private float mLastOffset;

    public PageTransformer(ViewPager viewPager, DishCardMainSiteAdapter dishCardMainSiteAdapter) {
        viewPager.addOnPageChangeListener(this);
        this.dishCardMainSiteAdapter = dishCardMainSiteAdapter;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int realCurrentPosition;
        int nextPosition;
        float baseElevation = dishCardMainSiteAdapter.getBaseElevation();
        float realOffset;
        boolean goingLeft = mLastOffset > positionOffset;

        if (goingLeft) {
            realCurrentPosition = position + 1;
            nextPosition = position;
            realOffset = 1 - positionOffset;
        } else {
            nextPosition = position + 1;
            realCurrentPosition = position;
            realOffset = positionOffset;
        }

//         Avoid crash on overscroll
        if (nextPosition > dishCardMainSiteAdapter.getCount() - 1
                || realCurrentPosition > dishCardMainSiteAdapter.getCount() - 1) {
            return;
        }

        CardView currentCard = dishCardMainSiteAdapter.getCardViewAt(realCurrentPosition);

        if (currentCard != null) {
            currentCard.setCardElevation((baseElevation + baseElevation
                    * (DishCardMainSiteAdapter.MAX_ELEVATION - 1) * (1 - realOffset)));
        }

        CardView nextCard = dishCardMainSiteAdapter.getCardViewAt(nextPosition);

        if (nextCard != null) {
            nextCard.setCardElevation((baseElevation + baseElevation
                    * (DishCardMainSiteAdapter.MAX_ELEVATION - 1) * (realOffset)));
        }

        mLastOffset = positionOffset;
    }

    @Override
    public void onPageSelected(int i) {
    }
    @Override
    public void onPageScrollStateChanged(int i) {
    }
    @Override
    public void transformPage(@NonNull View view, float v) {
    }
}
