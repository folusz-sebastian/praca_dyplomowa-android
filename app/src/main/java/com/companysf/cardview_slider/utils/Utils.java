package com.companysf.cardview_slider.utils;

import android.text.TextUtils;
import android.util.Patterns;

public class Utils {
    public static boolean isEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }
}
