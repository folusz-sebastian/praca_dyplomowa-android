package com.companysf.cardview_slider.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class CurrencyTextWatcher implements TextWatcher {
    private final EditText editText;
    private String previousCleanString;
    private String prefix;
    private int maxLength;
    private int maxDecimal;

    public CurrencyTextWatcher(EditText editText, String prefix, int maxLength, int maxDecimal) {
        this.editText = editText;
        this.prefix = prefix;
        this.maxLength = maxLength;
        this.maxDecimal = maxDecimal;
    }

    public void afterTextChanged(Editable editable) {
        String str = editable.toString();
        if (str.length() < prefix.length()) {
            editText.setText(prefix);
            editText.setSelection(prefix.length());
            return;
        }
        if (str.equals(prefix)) {
            return;
        }
        String cleanString = str.replace(prefix, "").replaceAll("[,]", "");

        if (cleanString.equals(previousCleanString) || cleanString.isEmpty()) {
            return;
        }
        previousCleanString = cleanString;

        String formattedString;
        if (cleanString.contains(".")) {
            formattedString = formatDecimal(cleanString);
        } else {
            formattedString = formatInteger(cleanString);
        }
        editText.removeTextChangedListener(this);
        editText.setText(formattedString);
        handleSelection();
        editText.addTextChangedListener(this);
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    public void onTextChanged(CharSequence s, int start, int before, int count) { }

    private String formatInteger(String str) {
        BigDecimal parsed = new BigDecimal(str);
        DecimalFormat formatter =
                new DecimalFormat(prefix + "#,###", new DecimalFormatSymbols(Locale.US));
        return formatter.format(parsed);
    }

    private String formatDecimal(String str) {
        if (str.equals(".")) {
            return prefix + ".";
        }
        BigDecimal parsed = new BigDecimal(str);
        DecimalFormat formatter = new DecimalFormat(prefix + "#,###." + getDecimalPattern(str),
                new DecimalFormatSymbols(Locale.US));
        formatter.setRoundingMode(RoundingMode.DOWN);
        return formatter.format(parsed);
    }

    private String getDecimalPattern(String str) {
        int decimalCount = str.length() - str.indexOf(".") - 1;
        StringBuilder decimalPattern = new StringBuilder();
        for (int i = 0; i < decimalCount && i < maxDecimal; i++) {
            decimalPattern.append("0");
        }
        return decimalPattern.toString();
    }

    private void handleSelection() {
        if (editText.getText().length() <= maxLength) {
            editText.setSelection(editText.getText().length());
        } else {
            editText.setSelection(maxLength);
        }
    }

}
