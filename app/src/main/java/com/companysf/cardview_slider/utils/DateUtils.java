package com.companysf.cardview_slider.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static final Date TOMORROW = dateCountedFromToday(1);
    public static final Date DAY_AFTER_TOMORROW = dateCountedFromToday(2);

    private static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        return isSameDay(calendar1, calendar2);
    }

    private static boolean isSameDay(Calendar calendar1, Calendar calendar2) {
        if (calendar1 == null || calendar2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (calendar1.get(Calendar.ERA) == calendar2.get(Calendar.ERA) &&
                calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR));
    }

    public static boolean isTomorrow(Calendar calendar2) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        return isSameDay(calendar2.getTime(), tomorrow);
    }

    public static boolean isDayAfterTomorrow(Calendar calendar2) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date dayAfterTomorrow = calendar.getTime();
        return isSameDay(calendar2.getTime(), dayAfterTomorrow);
    }

    public static Calendar setDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar;
    }

    private static Date dateCountedFromToday(int daysAfterToday) {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, daysAfterToday);
        return calendar.getTime();
    }
}
