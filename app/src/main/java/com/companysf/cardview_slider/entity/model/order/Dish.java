package com.companysf.cardview_slider.entity.model.order;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Dish {
    private long id;
    private String name;
    private double price;

    @SerializedName("image_url")
    private String imageUrl;

    private String description;
    private DishCategory category;
}
