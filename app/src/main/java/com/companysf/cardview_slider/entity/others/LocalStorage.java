package com.companysf.cardview_slider.entity.others;

import android.net.Uri;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public class LocalStorage {
    private Uri uri;
}
