package com.companysf.cardview_slider.entity.model.restaurantTable.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TablePlaceRealm extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;
    private boolean emptyName;
}
