package com.companysf.cardview_slider.entity.model.restaurantTable;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class TablePlace implements Parcelable {
    private long id;
    private String name;
    private boolean emptyName;

    protected TablePlace(Parcel in) {
        id = in.readLong();
        name = in.readString();
        emptyName = in.readByte() != 0;
    }

    public static final Creator<TablePlace> CREATOR = new Creator<TablePlace>() {
        @Override
        public TablePlace createFromParcel(Parcel in) {
            return new TablePlace(in);
        }

        @Override
        public TablePlace[] newArray(int size) {
            return new TablePlace[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeByte((byte) (emptyName ? 1 : 0));
    }
}
