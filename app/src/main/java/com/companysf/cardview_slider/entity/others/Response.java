package com.companysf.cardview_slider.entity.others;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public class Response {
    private boolean error;
    private String message;
}
