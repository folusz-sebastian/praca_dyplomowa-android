package com.companysf.cardview_slider.entity.model.order.postMethodBody;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public class OrderStatusOnlyName {
    private String name;
}
