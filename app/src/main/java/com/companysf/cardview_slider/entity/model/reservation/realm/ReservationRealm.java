package com.companysf.cardview_slider.entity.model.reservation.realm;

import com.companysf.cardview_slider.entity.model.restaurantTable.realm.RestaurantTableRealm;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReservationRealm extends RealmObject {
    @PrimaryKey
    private long id;
    private Date date;

    @SerializedName("beginning_hour")
    private int beginningHour;

    private int duration;
    private RestaurantTableRealm table;
}
