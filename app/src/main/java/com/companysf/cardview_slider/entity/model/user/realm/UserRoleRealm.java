package com.companysf.cardview_slider.entity.model.user.realm;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserRoleRealm extends RealmObject {
    private long id;
    private String role;
}
