package com.companysf.cardview_slider.entity.model.order;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DishOrder2 {
    private long id;
    private Dish dish;
    private int quantity;
    private Order2 order;
}
