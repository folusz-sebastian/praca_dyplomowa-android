package com.companysf.cardview_slider.entity.ui;

import android.content.res.ColorStateList;
import android.widget.TextView;

import lombok.Getter;
import lombok.Setter;

public class TextViewWithDefaultColor {
    @Getter @Setter
    private TextView textView;
    @Getter
    private ColorStateList defaultColor;

    public TextViewWithDefaultColor(TextView textView) {
        this.textView = textView;
        this.defaultColor = textView.getTextColors();
    }
}
