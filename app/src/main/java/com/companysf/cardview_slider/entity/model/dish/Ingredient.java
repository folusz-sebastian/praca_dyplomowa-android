package com.companysf.cardview_slider.entity.model.dish;


import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
public class Ingredient extends RealmObject {
    private long id;
    private String name;

    @SerializedName("measurement_unit")
    private MeasurementUnit measurementUnit;

    public Ingredient(String name, MeasurementUnit measurementUnit) {
        this.name = name;
        this.measurementUnit = measurementUnit;
    }

    public Ingredient() {
    }
}
