package com.companysf.cardview_slider.entity.model.order;

import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.google.gson.annotations.SerializedName;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Order3 {
    private long id;
    private double cost;
    private OrderStatus status;

    @SerializedName("dish_orders")
    private List<DishOrder3> dishOrders;

    private Reservation reservation;
}
