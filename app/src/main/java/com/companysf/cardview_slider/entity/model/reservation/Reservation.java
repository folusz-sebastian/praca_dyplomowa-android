package com.companysf.cardview_slider.entity.model.reservation;

import com.companysf.cardview_slider.app.Restaurant;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Reservation {
    private static final int INITIAL_DURATION = 1;

    @Getter @Setter
    private long id;
    @Getter @Setter
    private Date date;
    @Getter @Setter
    @SerializedName("beginning_hour")
    private int beginningHour;
    @Getter @Setter
    private int duration;
    @Getter @Setter
    private RestaurantTable table;

    public Reservation() {
        this.id = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        this.date = calendar.getTime();
        this.beginningHour = Restaurant.STARTING_JOB_HOUR;
        this.duration = INITIAL_DURATION;
    }

    public Reservation(long id) {
        this.id = id;
    }
}
