package com.companysf.cardview_slider.entity.model.user.realm;

import io.realm.RealmObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LoginSessionRoleRealm extends RealmObject {
    String name;

    public LoginSessionRoleRealm() {
    }

    public LoginSessionRoleRealm(String name) {
        this.name = name;
    }
}
