package com.companysf.cardview_slider.entity.model.user;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RegisteredUser {
    private String name;

    @SerializedName("last_name")
    private String lastName;

    private String email;
    private String password;
}
