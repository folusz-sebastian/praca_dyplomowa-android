package com.companysf.cardview_slider.entity.model.dish;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
public class DishIngredient extends RealmObject {
    private long id;
    private Ingredient ingredient;
    private double quantity;

    public DishIngredient(Ingredient ingredient, double quantity) {
        this.ingredient = ingredient;
        this.quantity = quantity;
    }

    public DishIngredient() {
    }
}
