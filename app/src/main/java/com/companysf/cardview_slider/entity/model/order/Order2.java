package com.companysf.cardview_slider.entity.model.order;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Order2 {
    private long id;
    private Date dateTime;
    private double cost;
    private OrderStatus status;
}
