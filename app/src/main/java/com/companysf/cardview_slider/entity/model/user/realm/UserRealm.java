package com.companysf.cardview_slider.entity.model.user.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserRealm extends RealmObject {
    @PrimaryKey
    private long id;
    private String email;
    private String name;

    @SerializedName("last_name")
    private String lastName;

    private RealmList<UserRoleRealm> roles;
}
