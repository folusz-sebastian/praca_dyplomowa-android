package com.companysf.cardview_slider.entity.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public class LoginForm {
    private String email;
    private String password;
}
