package com.companysf.cardview_slider.entity.model.order.Realm;

import com.companysf.cardview_slider.entity.model.dish.Dish;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RealmDishOrder extends RealmObject {
    @PrimaryKey
    private String id;
    private Dish dish;
    private int quantity;

    public RealmDishOrder() {
        this.id = UUID.randomUUID().toString();
        this.quantity = 0;
    }

    @Override
    public String toString() {
        return "RealmDishOrder{" +
                "id='" + id + '\'' +
                ", dish=" + dish.toString() +
                ", quantity=" + quantity +
                '}';
    }
}
