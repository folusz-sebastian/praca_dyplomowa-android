package com.companysf.cardview_slider.entity.model.dish;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter @Setter
public class Dish extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;
    private double price;
    private String description;

    @SerializedName("image_url")
    private String imageUrl;
    private DishCategory category;

    @SerializedName("dish_ingredients")
    private RealmList<DishIngredient> dishIngredients;

    public Dish() {
    }
}
