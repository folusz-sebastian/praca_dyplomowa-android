package com.companysf.cardview_slider.entity.model.restaurantTable.realm;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestaurantTableRealm extends RealmObject {
    @PrimaryKey
    private long id;
    private int chairs;
    private TablePlaceRealm place;
}
