package com.companysf.cardview_slider.entity.model.dish;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DishCategory extends RealmObject {
    private long id;
    private String name;

    public DishCategory() {
    }

    public DishCategory(String name) {
        this.name = name;
    }
}
