package com.companysf.cardview_slider.entity.model.order.postMethodBody;

import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class Order {
    private double cost;
    private OrderStatusOnlyName status;

    @SerializedName("dish_orders")
    private List<DishOrder> dishOrders;

    private Reservation reservation;
}
