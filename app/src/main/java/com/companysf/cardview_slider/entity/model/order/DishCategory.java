package com.companysf.cardview_slider.entity.model.order;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DishCategory {
    private long id;
    private String name;
}
