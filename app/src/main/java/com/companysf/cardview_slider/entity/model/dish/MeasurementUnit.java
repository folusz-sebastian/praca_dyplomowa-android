package com.companysf.cardview_slider.entity.model.dish;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
public class MeasurementUnit extends RealmObject {
    private long id;
    private String name;

    public MeasurementUnit() {
    }
}
