package com.companysf.cardview_slider.entity.model.order;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DishOrder3 {
    private long id;
    private Dish dish;
    private int quantity;
}
