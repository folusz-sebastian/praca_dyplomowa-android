package com.companysf.cardview_slider.entity.model.restaurantTable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RestaurantTable {
    private long id;
    private int chairs;
    private TablePlace place;

    @Override
    public String toString() {
        return "RestaurantTable{" +
                "id=" + id +
                ", chairs=" + chairs +
                ", place id and name=" + place.getId() + ", " + place.getName() +
                '}';
    }
}
