package com.companysf.cardview_slider.entity.model.order.postMethodBody;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter @Getter
public class DishOrder {
    private DishOnlyId dish;
    private int quantity;
}
