package com.companysf.cardview_slider.connection;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.Activity.mainActivity.CustomerMainActivity;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.adapter.DishCardMainSiteAdapter;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.dish.Dish;
import com.companysf.cardview_slider.entity.model.dish.DishIngredient;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;

public class DishConnection {
    private List<Dish> dishList;
    private Dish dish;
    private List<DishIngredient> dishIngredientList;
    private DishListener dishListener;
    private DishCardMainSiteAdapter dishCardMainSiteAdapter;
    private RecyclerView.Adapter dishCardAdapter;
    private RecyclerView.Adapter dishIngredientAdapter;
    private Gson gson;
    private static final String LOG_TAG = CustomerMainActivity.class.getSimpleName();
    private DishSavingListener dishSavingListener;

    public DishConnection(DishSavingListener dishSavingListener) {
        this.gson = new Gson();
        this.dishSavingListener = dishSavingListener;
    }

    public DishConnection(DishCardMainSiteAdapter dishCardMainSiteAdapter) {
        this.dishList = new ArrayList<>();
        this.dishCardMainSiteAdapter = dishCardMainSiteAdapter;
        this.gson = new Gson();
    }

    public DishConnection(List<Dish> dishList, RecyclerView.Adapter dishCardAdapter) {
        this.dishList = dishList;
        this.dishCardAdapter = dishCardAdapter;
        this.gson = new Gson();
    }

    public DishConnection(List<DishIngredient> dishIngredientList, RecyclerView.Adapter dishIngredientAdapter, DishListener dishListener) {
        this.dishIngredientList = dishIngredientList;
        this.gson = new Gson();
        this.dishIngredientAdapter = dishIngredientAdapter;
        this.dishListener = dishListener;
    }

    public void getDishes(final boolean all, String category) {
        Log.d(LOG_TAG, "category: " + category);
        String endpoint;
        if (all) {
            if (category != null) {
                endpoint = AppConfig.dishesUrl + "?category=" + category;
            } else {
                endpoint = AppConfig.dishesUrl;
            }
        } else {
            endpoint = AppConfig.dishesUrl + "?elements=" + AppConfig.NUMBER_OF_DISHES_ON_MAIN_SITE;
        }

        Log.d(LOG_TAG, "endpoint: " + endpoint);

        StringRequest stringRequest = new StringRequest(
                GET,
                endpoint,
                response -> {
                    Log.d(LOG_TAG, "getting dishes");

                    Dish[] dishes = this.gson.fromJson(
                            response,
                            Dish[].class
                    );
                    this.dishList.clear();
                    List<Dish> tempList = Arrays.asList(dishes);
                    this.dishList.addAll(tempList);

                    if (all) {
                        dishCardAdapter.notifyDataSetChanged();
                    } else {
                        dishCardMainSiteAdapter.addCardItems(this.dishList);
                        dishCardMainSiteAdapter.notifyDataSetChanged();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "Error on response");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void getDishes(long id) {

        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.dishesUrl + "/" + id,
                response -> {
                    Log.d(LOG_TAG, "getting dish with id: " + id);

                    this.dish = this.gson.fromJson(
                            response,
                            Dish.class
                    );
                    this.dishIngredientList.addAll(this.dish.getDishIngredients());
                    Log.d(LOG_TAG, "dish with response: " + response);
                    Log.d(LOG_TAG, "dish with dish.toString: " + dish.toString());

                    this.dishIngredientAdapter.notifyDataSetChanged();
                    dishListener.onGettingDish(this.dish);
                },
                error -> {
                    Log.d(LOG_TAG, "Error on response");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void getDishes(String category) {
        getDishes(true, category);
    }

    public void saveDish(Dish body) {
        Log.d(LOG_TAG, "method to save dish to DB");
        Log.d(LOG_TAG, "dish body: " + gson.toJson(body));

        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.dishesUrl,
                response -> {
                    Log.d(LOG_TAG, "dish was saved successfully");
                    try {
                        Log.d(LOG_TAG, "posting response: " + response);
                        this.dishSavingListener.onSuccessSavingDish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on saving order");
                    this.dishSavingListener.onErrorSavingDish();
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface DishListener {
        void onGettingDish(Dish dish);
    }

    public interface DishSavingListener {
        void onSuccessSavingDish();
        void onErrorSavingDish();
    }
}
