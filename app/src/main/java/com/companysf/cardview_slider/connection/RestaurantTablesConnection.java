package com.companysf.cardview_slider.connection;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.Activity.RestaurantTablesListActivity;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import solid.optional.Optional;
import static com.android.volley.Request.Method.DELETE;
import static com.android.volley.Request.Method.GET;
import static solid.stream.Stream.stream;

public class RestaurantTablesConnection {
    private List<RestaurantTable> restaurantTableList;
    private RecyclerView.Adapter adapter;
    private Gson gson;
    private static final String LOG_TAG = RestaurantTablesListActivity.class.getSimpleName();

    public RestaurantTablesConnection(List<RestaurantTable> restaurantTableList, RecyclerView.Adapter adapter) {
        this.restaurantTableList = restaurantTableList;
        this.adapter = adapter;
        gson = new Gson();
    }

    public void findAll() {
        Log.d(LOG_TAG, "findAll method");

        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.restaurantTablesUrl,
                response -> {
                    Log.d(LOG_TAG, "getting restaurant tables");
                    try {
                        RestaurantTable[] restaurantTables = gson.fromJson(
                                response,
                                RestaurantTable[].class
                        );
                        this.restaurantTableList.clear();
                        List<RestaurantTable> tempList = Arrays.asList(restaurantTables);
                        this.restaurantTableList.addAll(tempList);
                        for (RestaurantTable restaurantTable : restaurantTableList) {
                            Log.d(LOG_TAG, "table: " + restaurantTable.toString());
                        }
                        this.adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on getting restaurant tables");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void findAll(String year, String month, String day, int beginningHour, int stayDuration) {
        Log.d(LOG_TAG, "my Parameters: ");
        Log.d(LOG_TAG, year + ", " + month + ", " + day + ", " + beginningHour + ", " + stayDuration);

        String url = AppConfig.freeRestaurantTablesUrl +
                "?year=" + year +
                "&month=" + month +
                "&day=" + day +
                "&beginning_hour=" + beginningHour +
                "&duration=" + stayDuration;

        StringRequest stringRequest = new StringRequest(
                GET,
                url,
                response -> {
                    Log.d(LOG_TAG, "getting free restaurant tables");
                    try {
                        RestaurantTable[] restaurantTables = gson.fromJson(
                                response,
                                RestaurantTable[].class
                        );
                        this.restaurantTableList.clear();
                        List<RestaurantTable> tempList = Arrays.asList(restaurantTables);
                        this.restaurantTableList.addAll(tempList);
                        for (RestaurantTable restaurantTable : restaurantTableList) {
                            Log.d(LOG_TAG, "table: " + restaurantTable.toString());
                        }
                        this.adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on getting free restaurant tables");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void delete(long id) {
        StringRequest stringRequest = new StringRequest(
                DELETE,
                AppConfig.restaurantTablesUrl + "/" + id,
                response -> {
                    Log.d(LOG_TAG, "removing restaurant tables");
                    try {
                        Optional<RestaurantTable> firstElemWithSpecificId =
                                stream(this.restaurantTableList)
                                        .filter(value -> value.getId() == id)
                                        .first();

                        if (firstElemWithSpecificId.isPresent()) {
                            this.restaurantTableList.remove(firstElemWithSpecificId.get());
                        }

                        for (RestaurantTable restaurantTable : restaurantTableList) {
                            Log.d(LOG_TAG, "table: " + restaurantTable.toString());
                        }
                        this.adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on getting restaurant tables");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
