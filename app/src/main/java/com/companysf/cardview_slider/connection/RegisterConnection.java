package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.user.RegisteredUser;
import com.companysf.cardview_slider.entity.others.Response;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Request.Method.POST;

public class RegisterConnection {
    private static final String LOG_TAG = "register connection";
    private Gson gson;
    private RegisterListener listener;

    public RegisterConnection(RegisterListener listener) {
        this.gson = new Gson();
        this.listener = listener;
    }

    public void save(RegisteredUser body) {
        Log.d(LOG_TAG, "method to create new userEmail");

        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.userRegistrationUrl,
                response -> {
                    Log.d(LOG_TAG, "posting new userEmail");
                    Log.d(LOG_TAG, "posting response: " + response);
                    this.listener.onResponse();
                },
                error -> {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String responseString = new String(networkResponse.data);
                        Response response = gson.fromJson(responseString, Response.class);
                        response.setError(true);
                        Log.d(LOG_TAG, "error on saving userEmail. " +
                                "Error: " + response.isError() +
                                ", Response " + response.getMessage());
                        this.listener.onError(response.getMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface RegisterListener {
        void onResponse();
        void onError(String message);
    }
}
