package com.companysf.cardview_slider.connection;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.dish.MeasurementUnit;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.android.volley.Request.Method.GET;

public class MeasurementUnitConnection {
    private MeasurementUnitListener listener;
    private List<MeasurementUnit> measurementUnitList;
    private Gson gson;
    private static final String LOG_TAG = "MeasurementUnitConn";

    public MeasurementUnitConnection(MeasurementUnitListener listener, List<MeasurementUnit> measurementUnitList) {
        this.listener = listener;
        this.measurementUnitList = measurementUnitList;
        this.gson = new Gson();
    }

    public void getMeasurementUnits() {
        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.measurementUnits,
                response -> {
                    Log.d(LOG_TAG, "getting measurement units");

                    MeasurementUnit[] measurementUnits = this.gson.fromJson(
                            response,
                            MeasurementUnit[].class
                    );
                    this.measurementUnitList.clear();
                    List<MeasurementUnit> tempList = Arrays.asList(measurementUnits);
                    this.measurementUnitList.addAll(tempList);

                    this.listener.onSuccessGettingMeasurementUnits();
                },
                error -> {
                    Log.d(LOG_TAG, "Error on response");
                    this.listener.onFailureGettingMeasurementUnits();
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface MeasurementUnitListener {
        void onSuccessGettingMeasurementUnits();
        void onFailureGettingMeasurementUnits();
    }
}
