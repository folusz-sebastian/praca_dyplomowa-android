package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.user.LoginForm;
import com.companysf.cardview_slider.entity.model.user.realm.UserRealm;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.codec.binary.Base64;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;

public class UserConnection {
    private static final String LOG_TAG = "login connection";
    private Gson gson;
    private LoginListener listener;
    private String authenticationKey;

    public UserConnection(LoginListener listener, String dateFormat) {
        this.listener = listener;
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
    }

    public UserConnection() {
        this.gson = new Gson();
    }

    public void getUser(LoginForm body) {
        this.authenticationKey = encodePasswordAndUser(body);
        Log.d("UserConnection", "getting user body: " + gson.toJson(body));

        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.userSignInUrl,
                response -> {
                    Log.d(LOG_TAG, "getting userRealm");

                    UserRealm userRealm = this.gson.fromJson(
                            response,
                            UserRealm.class
                    );
                    Log.d("UserConnection", "user from backend" + response);
                    Log.d("UserConnection", "user from realm" + gson.toJson(userRealm));

                    this.listener.onResponse(userRealm, this.authenticationKey);
                },
                error -> {
                    Log.d(LOG_TAG, "Error on response");

                    NetworkResponse networkResponse = error.networkResponse;

                    this.listener.onError(networkResponse);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", authenticationKey);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private String encodePasswordAndUser(LoginForm loginForm) {
        String userAndPassword = loginForm.getEmail() + ":" + loginForm.getPassword();
        String s = new String(Base64.encodeBase64(userAndPassword.getBytes()));
        return "Basic " + s;
    }

    public void logoutUser() {
        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.logoutUserUrl,
                response -> {
                    Log.d(LOG_TAG, "user logged out successfully: " + response);
                },
                error -> {
                    Log.d(LOG_TAG, "error on logging out user");
                }
        );
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface LoginListener {
        void onResponse(UserRealm userRealm, String authenticationKey);
        void onError(NetworkResponse networkResponse);
    }

}
