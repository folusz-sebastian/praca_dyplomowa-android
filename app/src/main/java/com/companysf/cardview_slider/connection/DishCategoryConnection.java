package com.companysf.cardview_slider.connection;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.dish.DishCategory;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.android.volley.Request.Method.GET;

public class DishCategoryConnection {
    private List<DishCategory> dishCategoryList;
    private DishCategoryListener listener;
    private Gson gson;
    private static final String LOG_TAG = "DishCategoryConnection";

    public DishCategoryConnection(List<DishCategory> dishCategoryList, DishCategoryListener listener) {
        this.dishCategoryList = dishCategoryList;
        this.listener = listener;
        this.gson = new Gson();
    }

    public void getDishCategories() {
        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.dishCategories,
                response -> {
                    Log.d(LOG_TAG, "getting dish categories");

                    DishCategory[] dishCategories = this.gson.fromJson(
                            response,
                            DishCategory[].class
                    );
                    this.dishCategoryList.clear();
                    List<DishCategory> tempList = Arrays.asList(dishCategories);
                    this.dishCategoryList.addAll(tempList);

                    this.listener.onSuccessGettingDishCategories();
                },
                error -> {
                    Log.d(LOG_TAG, "Error on response");
                    this.listener.onFailureGettingDishCategories();
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface DishCategoryListener {
        void onSuccessGettingDishCategories();
        void onFailureGettingDishCategories();
    }
}
