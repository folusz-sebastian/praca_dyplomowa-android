package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.Activity.mainActivity.CustomerMainActivity;
import com.companysf.cardview_slider.adapter.LastTransactionsAdapter;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.order.DishOrder2;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.GET;

public class LastTransactionsConnection {
    private List<DishOrder2> lastTransactionList;
    private LastTransactionsAdapter adapter;
    private static final String LOG_TAG = "LastTransactionsConn";
    private Gson gson;

    public LastTransactionsConnection(String dateFormat,
                                      List<DishOrder2> lastTransactionList,
                                      LastTransactionsAdapter adapter) {
        this.lastTransactionList = lastTransactionList;
        this.adapter = adapter;
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
    }

    public void getLastTransactions(boolean all) {
        String endpoint = all ?
                AppConfig.lastUserTransactionsUrl :
                AppConfig.lastUserTransactionsUrl +
                        "&elements=" + AppConfig.NUMBER_OF_LAST_TRANSACTIONS_ON_MAIN_SITE;

        StringRequest stringRequest = new StringRequest(
                GET,
                endpoint,
                response -> {
                    Log.d(LOG_TAG, "get last transactions request");
                    DishOrder2[] dishOrders = gson.fromJson(response, DishOrder2[].class);
                    this.lastTransactionList.clear();
                    List<DishOrder2> tempList = Arrays.asList(dishOrders);
                    this.lastTransactionList.addAll(tempList);
                    this.adapter.notifyDataSetChanged();
                },
                error -> {
                    Log.d(LOG_TAG, "last transactions - Error on response");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    public void getLastTransactions() {
        getLastTransactions(true);
    }
}
