package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.order.Order3;
import com.companysf.cardview_slider.entity.model.order.OrderStatus;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.PUT;

public class OrderConnection {
    private Gson gson;
    private List<Order3> orderList;
    private OrdersListener ordersListener;

    private Order3 order;
    private OrderListener orderListener;
    private final static String LOG_TAG = "OrderConnection";

    public OrderConnection(List<Order3> orderList, OrdersListener ordersListener, String dateFormat) {
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
        this.ordersListener = ordersListener;
        this.orderList = orderList;
    }

    public OrderConnection(Order3 order, OrderListener orderListener, String dateFormat) {
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
        this.orderListener = orderListener;
        this.order = order;
    }

    public void findById(Long orderId) {
        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.orderUrl + "/" + orderId,
                response -> {
                    Log.d(LOG_TAG, "on response finding order by id");
                    try {
                        Log.d(LOG_TAG, "order by id response: " + response);
                        this.order = gson.fromJson(
                                response,
                                Order3.class
                        );

                        this.orderListener.onGettingOrder(this.order);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on finding order by id");
                    Log.d(LOG_TAG, error.getMessage());
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void findAll() {
        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.ordersNotPrepared,
                response -> {
                    Log.d(LOG_TAG, "on response finding orders not prepared");
                    try {
                        Log.d(LOG_TAG, "orders not prepared response: " + response);
                        Order3[] orders = gson.fromJson(
                                response,
                                Order3[].class
                        );
                        this.orderList.clear();
                        List<Order3> tempList = Arrays.asList(orders);
                        this.orderList.addAll(tempList);

                        this.ordersListener.onGettingOrders();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on finding orders not prepared");
                    Log.d(LOG_TAG, error.getMessage());
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void updateOrderStatus(long orderID, String status) {
        Order3 body = new Order3();
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setName(status);
        body.setStatus(orderStatus);

        StringRequest stringRequest = new StringRequest(
                PUT,
                AppConfig.orderUrl + "/" + orderID,
                response -> {
                    try {
                        Log.d(LOG_TAG, "order with changed status: " + response);

                        this.orderListener.onChangingOrderStatus();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on finding orders not prepared: " + error.getMessage());
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface OrdersListener {
        void onGettingOrders();
    }

    public interface OrderListener {
        void onGettingOrder(Order3 order);
        void onChangingOrderStatus();
    }


}
