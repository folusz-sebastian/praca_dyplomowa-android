package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.Activity.RestaurantTablesListActivity;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.reservation.Reservation;
import com.companysf.cardview_slider.entity.model.reservation.realm.ReservationRealm;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;

public class ReservationConnection {
    private static final String LOG_TAG = RestaurantTablesListActivity.class.getSimpleName();
    private Gson gson;
    private ReservationListener reservationListener;
    private ReservationsListener reservationsListener;
    private List<Reservation> reservationList;

    public ReservationConnection(ReservationListener reservationListener, String dateFormat) {
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
        this.reservationListener = reservationListener;
    }

    public ReservationConnection(List<Reservation> reservationList,
                                 ReservationsListener reservationsListener, String dateFormat) {
        this.gson = new GsonBuilder().setDateFormat(dateFormat).create();
        this.reservationList = reservationList;
        this.reservationsListener = reservationsListener;
    }

    public void save(Reservation body) {
        Log.d(LOG_TAG, "saving reservation, body: " + gson.toJson(body));

        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.reservationsUrl,
                response -> {
                    Log.d(LOG_TAG, "on response saving reservation");
                    try {
                        Log.d(LOG_TAG, "posting response: " + response);
                        this.reservationListener.onGettingReservation(
                                gson.fromJson(response, ReservationRealm.class)
                        );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on saving reservation");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void findByUser() {
        StringRequest stringRequest = new StringRequest(
                GET,
                AppConfig.myReservationsUrl,
                response -> {
                    Log.d(LOG_TAG, "on response finding reservations by userEmail");
                    try {
                        Log.d(LOG_TAG, "reservations by userEmail response: " + response);
                        Reservation[] reservations = gson.fromJson(
                                response,
                                Reservation[].class
                        );
                        this.reservationList.clear();
                        List<Reservation> tempList = Arrays.asList(reservations);
                        this.reservationList.addAll(tempList);

                        this.reservationsListener.onGettingReservations();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on finding reservations by userEmail");
                    Log.d(LOG_TAG, error.getMessage());
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface ReservationListener {
        void onGettingReservation(ReservationRealm body);
    }

    public interface ReservationsListener {
        void onGettingReservations();
    }
}
