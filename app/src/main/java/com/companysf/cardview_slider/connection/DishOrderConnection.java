package com.companysf.cardview_slider.connection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.order.postMethodBody.Order;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Request.Method.POST;

public class DishOrderConnection {
    private static final String LOG_TAG = "order";
    private Gson gson;
    private OrderListener listener;

    public DishOrderConnection(OrderListener listener) {
        this.gson = initGson();
        this.listener = listener;
    }

    public void save(Order body) {
        Log.d(LOG_TAG, "method to save order to DB");
        Log.d(LOG_TAG, "order body: " + gson.toJson(body));

        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.orderUrl,
                response -> {
                    Log.d(LOG_TAG, "order was saved successfully");
                    try {
                        Log.d(LOG_TAG, "posting response: " + response);
                        this.listener.onSuccessSavingOrder();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on saving order");
                    this.listener.onErrorSavingOrder();
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public interface OrderListener {
        void onSuccessSavingOrder();
        void onErrorSavingOrder();
    }

    private Gson initGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
            DecimalFormat df = new DecimalFormat("#.##");
            DecimalFormatSymbols dfs = new DecimalFormatSymbols();
            dfs.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(dfs);
            df.setRoundingMode(RoundingMode.CEILING);
            return new JsonPrimitive(Double.parseDouble(df.format(src)));
        });
        return builder.create();
    }
}
