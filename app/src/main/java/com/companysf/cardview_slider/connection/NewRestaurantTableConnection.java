package com.companysf.cardview_slider.connection;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.companysf.cardview_slider.Activity.RestaurantTablesListActivity;
import com.companysf.cardview_slider.app.AppConfig;
import com.companysf.cardview_slider.app.AppController;
import com.companysf.cardview_slider.entity.model.restaurantTable.RestaurantTable;
import com.companysf.cardview_slider.services.UserService;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import static com.android.volley.Request.Method.POST;

public class NewRestaurantTableConnection {
    private static final String LOG_TAG = RestaurantTablesListActivity.class.getSimpleName();
    private Gson gson;
    private RestaurantTableListener restaurantTableListener;

    public NewRestaurantTableConnection(RestaurantTableListener restaurantTableListener) {
        this.restaurantTableListener = restaurantTableListener;
        this.gson = new Gson();
    }

    public void save(RestaurantTable body) {
        Log.d(LOG_TAG, "post RestaurantTables method");

        StringRequest stringRequest = new StringRequest(
                POST,
                AppConfig.restaurantTablesUrl,
                response -> {
                    Log.d(LOG_TAG, "posting new restaurant table");
                    try {
                        Log.d(LOG_TAG, "posting response: " + response);
                        this.restaurantTableListener.onGettingRestaurantTable(
                                gson.fromJson(response, RestaurantTable.class)
                        );

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Log.d(LOG_TAG, "error on getting restaurant tables");
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("authorization", UserService.getAuthKey());

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return gson.toJson(body).getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    public interface RestaurantTableListener {
        void onGettingRestaurantTable(RestaurantTable body);
    }
}
